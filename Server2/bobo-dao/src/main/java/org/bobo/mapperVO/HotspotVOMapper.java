package org.bobo.mapperVO;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.vo.HotspotVO;

/**
 * @Description: 热点表/热搜表 视图
 * @Author: boBo
 * @Date:   2022-03-26
 * @Version: V1.0
 */
public interface HotspotVOMapper extends BaseMapper<HotspotVO> {

}
