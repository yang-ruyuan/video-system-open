package org.bobo.mapperVO;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.vo.SpuVO;

/**
 * @Description: 商品视图
 * @Author: boBo
 * @Date:   2022-04-05
 * @Version: V1.0
 */
public interface SpuVOMapper extends BaseMapper<SpuVO> {
    List<SpuVO> getListByCategoryId(@Param("categoryId") Integer categoryId);
}
