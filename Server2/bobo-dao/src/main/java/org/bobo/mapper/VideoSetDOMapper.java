package org.bobo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.bobo.bean.VideoSetDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.vo.VideoSetVO;

import java.util.List;

/**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date: 2023-04-09
 * @Version: V1.0
 */
public interface VideoSetDOMapper extends BaseMapper<VideoSetDO> {

    List<VideoSetVO> customList(@Param("ew") LambdaQueryWrapper<VideoSetDO> lambdaQueryWrapper);

    IPage<VideoSetVO> customPage(Page<VideoSetDO> page, @Param("ew") LambdaQueryWrapper<VideoSetDO> lambdaQueryWrapper);

    List<VideoSetVO> videoSetHostList(@Param("ew") QueryWrapper<VideoSetDO> queryWrapper);

    List<VideoSetVO> videoSetHostListByWeek(@Param("ew") QueryWrapper<VideoSetDO> queryWrapper);

    List<VideoSetVO> findById(@Param("id") Long id);
}
