package org.bobo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.CommentLikeRecordDO;

/**
 * @Description: 评论点赞记录
 * @Author: boBo
 * @Date:   2023-06-17
 * @Version: V1.0
 */
public interface CommentLikeRecordDOMapper extends BaseMapper<CommentLikeRecordDO> {

}
