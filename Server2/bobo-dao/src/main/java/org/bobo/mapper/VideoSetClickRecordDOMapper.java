package org.bobo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.VideoSetClickRecordDO;

/**
 * @Description: 专辑点击记录
 * @Author: boBo
 * @Date:   2023-07-23
 * @Version: V1.0
 */
public interface VideoSetClickRecordDOMapper extends BaseMapper<VideoSetClickRecordDO> {

}
