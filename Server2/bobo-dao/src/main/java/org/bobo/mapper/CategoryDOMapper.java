package org.bobo.mapper;

import org.bobo.bean.CategoryDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 视频分类管理/侧边栏管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface CategoryDOMapper extends BaseMapper<CategoryDO> {

}
