package org.bobo.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.vo.ViewHistoryVO;

/**
 * @Description: 播放历史记录管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface ViewHistoryDOMapper extends BaseMapper<ViewHistoryDO> {

    IPage<ViewHistoryVO> customPage(Page<ViewHistoryDO> page, @Param("ew") QueryWrapper<ViewHistoryDO> queryWrapper);

    Long getHistoryVideoByVideoSetId(@Param("videoSetId") Long videoSetId, @Param("userId") Integer userId);
}
