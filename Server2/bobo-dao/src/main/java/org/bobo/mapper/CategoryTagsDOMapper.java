package org.bobo.mapper;

import org.bobo.bean.CategoryTagsDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 视频分类标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface CategoryTagsDOMapper extends BaseMapper<CategoryTagsDO> {

}
