package org.bobo.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.CollectDO;
import org.bobo.vo.CollectVO;

/**
 * @Description: 收藏管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface CollectDOMapper extends BaseMapper<CollectDO> {
    /**
     * 分页列表查询
     *
     * @param queryWrapper
     * @return
     */
    IPage<CollectVO> customPage(Page<CollectDO> page, @Param("ew") QueryWrapper<CollectDO> queryWrapper);
}
