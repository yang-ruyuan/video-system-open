package org.bobo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.UserDO;
import org.bobo.vo.UserVO;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
public interface UserDOMapper extends BaseMapper<UserDO> {
    UserVO findById(Integer id);
}
