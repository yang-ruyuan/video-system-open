package org.bobo.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.bobo.bean.ChasingDramaDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.vo.ChasingDramaVO;

/**
 * @Description: 追剧管理管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface ChasingDramaDOMapper extends BaseMapper<ChasingDramaDO> {

    ChasingDramaDO getByUserIdCommentId(@Param("setId") Integer setId, @Param("userId") Integer userId);

    IPage<ChasingDramaVO> customPage(Page<ChasingDramaDO> page, @Param(Constants.WRAPPER) QueryWrapper<ChasingDramaDO> queryWrapper);
}
