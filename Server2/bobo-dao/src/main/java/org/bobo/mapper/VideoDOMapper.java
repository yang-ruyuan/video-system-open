package org.bobo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.bobo.bean.VideoDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.vo.VideoVO;

import java.util.List;

/**
 * @Description: 视频管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface VideoDOMapper extends BaseMapper<VideoDO> {

    IPage<VideoVO> customPage(Page<VideoDO> page, @Param("ew") LambdaQueryWrapper<VideoDO> queryWrapper);

    List<VideoVO> customList(@Param("ew")  LambdaQueryWrapper<VideoDO> queryWrapper);
}
