package org.bobo.mapper;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.bobo.bean.CollectDO;
import org.bobo.bean.CommentDO;
import org.bobo.vo.CommentVO;

import java.util.List;

/**
 * @Description: 文章评论管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
public interface CommentDOMapper extends BaseMapper<CommentDO> {

    IPage<CommentVO> customPage(Page<CommentDO> page, @Param("ew") LambdaQueryWrapper<CommentDO> lambdaQueryWrapper, @Param("sort")String sort);

    int decrLike(Long commentId);

    int incrLike(Long commentId);


    int decrReply(Long commentId);

    int incrReply(Long commentId);
}
