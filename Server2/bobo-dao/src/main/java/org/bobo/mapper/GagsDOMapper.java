package org.bobo.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.bean.GagsDO;
import org.bobo.vo.GagsVO;

/**
 * @Description: 花絮管理
 * @Author: boBo
 * @Date:   2023-06-07
 * @Version: V1.0
 */
public interface GagsDOMapper extends BaseMapper<GagsDO> {

    IPage<GagsVO> customPage(Page<GagsDO> page, @Param("ew") LambdaQueryWrapper<GagsDO> lambdaQueryWrapper);
}
