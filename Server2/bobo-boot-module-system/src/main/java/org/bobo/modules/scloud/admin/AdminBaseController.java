package org.bobo.modules.scloud.admin;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.bobo.bean.CommentDO;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.service.ICommentDOService;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 文章评论管理
 * @Author: jeecg-boot
 * @Date: 2022-02-27
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "总后台——基础控制层")
@RestController
@RequestMapping("/admin/base")
public class AdminBaseController extends JeecgController<CommentDO, ICommentDOService> {


}
