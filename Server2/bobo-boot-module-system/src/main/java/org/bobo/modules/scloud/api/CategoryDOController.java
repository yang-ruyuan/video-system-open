package org.bobo.modules.scloud.api;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bobo.common.api.vo.Result;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.bean.CategoryDO;
import org.bobo.service.ICategoryDOService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.common.system.base.controller.JeecgController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 视频分类管理/侧边栏管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags="用户端——视频分类管理/侧边栏管理")
@RestController
@RequestMapping("/api/categoryDO")
public class CategoryDOController extends JeecgController<CategoryDO, ICategoryDOService> {
	@Autowired
	private ICategoryDOService categoryDOService;
	
	/**
	 * 分页列表查询
	 *
	 * @param categoryDO
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-分页列表查询")
	@ApiOperation(value="视频分类管理/侧边栏管理-分页列表查询", notes="视频分类管理/侧边栏管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CategoryDO categoryDO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CategoryDO> queryWrapper = QueryGenerator.initQueryWrapper(categoryDO, req.getParameterMap());
		Page<CategoryDO> page = new Page<CategoryDO>(pageNo, pageSize);
		IPage<CategoryDO> pageList = categoryDOService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param categoryDO
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-添加")
	@ApiOperation(value="视频分类管理/侧边栏管理-添加", notes="视频分类管理/侧边栏管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CategoryDO categoryDO) {
		categoryDOService.save(categoryDO);
		return Result.OK("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param categoryDO
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-编辑")
	@ApiOperation(value="视频分类管理/侧边栏管理-编辑", notes="视频分类管理/侧边栏管理-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody CategoryDO categoryDO) {
		categoryDOService.updateById(categoryDO);
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-通过id删除")
	@ApiOperation(value="视频分类管理/侧边栏管理-通过id删除", notes="视频分类管理/侧边栏管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		categoryDOService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-批量删除")
	@ApiOperation(value="视频分类管理/侧边栏管理-批量删除", notes="视频分类管理/侧边栏管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.categoryDOService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频分类管理/侧边栏管理-通过id查询")
	@ApiOperation(value="视频分类管理/侧边栏管理-通过id查询", notes="视频分类管理/侧边栏管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CategoryDO categoryDO = categoryDOService.getById(id);
		return Result.OK(categoryDO);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param categoryDO
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, CategoryDO categoryDO) {
      return super.exportXls(request, categoryDO, CategoryDO.class, "视频分类管理/侧边栏管理");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, CategoryDO.class);
  }

}
