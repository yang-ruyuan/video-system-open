package org.bobo.modules.scloud.admin;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.service.IViewHistoryDOService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 播放历史记录管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Slf4j
@Api(tags="总后台——播放历史记录管理")
@RestController
@RequestMapping("/admin/viewHistoryDO")
public class AdminViewHistoryDOController extends JeecgController<ViewHistoryDO, IViewHistoryDOService> {
	@Autowired
	private IViewHistoryDOService viewHistoryDOService;
	
	/**
	 * 分页列表查询
	 *
	 * @param viewHistoryDO
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-分页列表查询")
	@ApiOperation(value="播放历史记录管理-分页列表查询", notes="播放历史记录管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ViewHistoryDO viewHistoryDO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ViewHistoryDO> queryWrapper = QueryGenerator.initQueryWrapper(viewHistoryDO, req.getParameterMap());
		Page<ViewHistoryDO> page = new Page<ViewHistoryDO>(pageNo, pageSize);
		IPage<ViewHistoryDO> pageList = viewHistoryDOService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param viewHistoryDO
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-添加")
	@ApiOperation(value="播放历史记录管理-添加", notes="播放历史记录管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ViewHistoryDO viewHistoryDO) {
		viewHistoryDOService.save(viewHistoryDO);
		return Result.OK("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param viewHistoryDO
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-编辑")
	@ApiOperation(value="播放历史记录管理-编辑", notes="播放历史记录管理-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody ViewHistoryDO viewHistoryDO) {
		viewHistoryDOService.updateById(viewHistoryDO);
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-通过id删除")
	@ApiOperation(value="播放历史记录管理-通过id删除", notes="播放历史记录管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		viewHistoryDOService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-批量删除")
	@ApiOperation(value="播放历史记录管理-批量删除", notes="播放历史记录管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.viewHistoryDOService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "播放历史记录管理-通过id查询")
	@ApiOperation(value="播放历史记录管理-通过id查询", notes="播放历史记录管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ViewHistoryDO viewHistoryDO = viewHistoryDOService.getById(id);
		return Result.OK(viewHistoryDO);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param viewHistoryDO
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, ViewHistoryDO viewHistoryDO) {
      return super.exportXls(request, viewHistoryDO, ViewHistoryDO.class, "播放历史记录管理");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, ViewHistoryDO.class);
  }

}
