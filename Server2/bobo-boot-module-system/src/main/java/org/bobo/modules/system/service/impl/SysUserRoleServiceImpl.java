package org.bobo.modules.system.service.impl;

import org.bobo.modules.system.mapper.SysUserRoleMapper;
import org.bobo.modules.system.service.ISysUserRoleService;
import org.bobo.sys.entity.SysUserRole;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @Author scott
 * @since 2018-12-21
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
