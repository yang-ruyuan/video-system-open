package org.bobo.modules.scloud.admin;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bobo.common.api.vo.Result;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.bean.VideoDO;
import org.bobo.dto.VideoDTO;
import org.bobo.service.IVideoDOService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.common.system.base.controller.JeecgController;

import org.bobo.vo.VideoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 视频管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags="总后台——视频管理")
@RestController
@RequestMapping("/admin/videoDO")
public class AdminVideoDOController extends JeecgController<VideoDO, IVideoDOService> {
	@Autowired
	private IVideoDOService videoDOService;

	/**
	 * 分页列表查询
	 *
	 * @param videoDTO
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "视频管理-分页列表查询")
	@ApiOperation(value="视频管理-分页列表查询", notes="视频管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(VideoDTO videoDTO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		Page<VideoDO> page = new Page<VideoDO>(pageNo, pageSize);
		IPage<VideoVO> pageList = videoDOService.customPage(page, videoDTO);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param videoDTO
	 * @return
	 */
	@AutoLog(value = "视频管理-添加")
	@ApiOperation(value="视频管理-添加", notes="视频管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody @Validated VideoDTO videoDTO) {
		try {
			videoDOService.saveDTO(videoDTO);
		}catch (DuplicateKeyException e){
			throw new BusinessException(ExceptionEnums.VIDEO_SET_IS_EXIST);
		}
		return Result.OK("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param videoDO
	 * @return
	 */
	@AutoLog(value = "视频管理-编辑")
	@ApiOperation(value="视频管理-编辑", notes="视频管理-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody VideoDO videoDO) {
		try {
			videoDOService.updateById(videoDO);
		}catch (DuplicateKeyException e){
			throw new BusinessException(ExceptionEnums.VIDEO_SET_IS_EXIST);
		}
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频管理-通过id删除")
	@ApiOperation(value="视频管理-通过id删除", notes="视频管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		videoDOService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "视频管理-批量删除")
	@ApiOperation(value="视频管理-批量删除", notes="视频管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.videoDOService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频管理-通过id查询")
	@ApiOperation(value="视频管理-通过id查询", notes="视频管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		VideoDO videoDO = videoDOService.getById(id);
		return Result.OK(videoDO);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param videoDO
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, VideoDO videoDO) {
      return super.exportXls(request, videoDO, VideoDO.class, "视频管理");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, VideoDO.class);
  }

}
