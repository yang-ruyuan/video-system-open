package org.bobo.modules.scloud.admin;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bobo.bean.CommentDO;
import org.bobo.service.ICommentDOService;
import org.bobo.common.api.vo.Result;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* @Description: 文章评论管理
* @Author: jeecg-boot
* @Date:   2022-02-27
* @Version: V1.0
*/
@Slf4j
@Api(tags="总后台——文章评论管理")
@RestController
@RequestMapping("/admin/articleCommentDO")
public class AdminArticleCommentDOController extends JeecgController<CommentDO, ICommentDOService> {
   @Autowired
   private ICommentDOService articleCommentDOService;

   /**
    * 分页列表查询
    *
    * @param articleCommentDO
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "文章评论管理-分页列表查询")
   @ApiOperation(value="文章评论管理-分页列表查询", notes="文章评论管理-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(CommentDO articleCommentDO,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<CommentDO> queryWrapper = QueryGenerator.initQueryWrapper(articleCommentDO, req.getParameterMap());
       Page<CommentDO> page = new Page<CommentDO>(pageNo, pageSize);
       IPage<CommentDO> pageList = articleCommentDOService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    * 添加
    *
    * @param articleCommentDO
    * @return
    */
   @AutoLog(value = "文章评论管理-添加")
   @ApiOperation(value="文章评论管理-添加", notes="文章评论管理-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody CommentDO articleCommentDO) {
       articleCommentDOService.save(articleCommentDO);
       return Result.OK("添加成功！");
   }

   /**
    * 编辑
    *
    * @param articleCommentDO
    * @return
    */
   @AutoLog(value = "文章评论管理-编辑")
   @ApiOperation(value="文章评论管理-编辑", notes="文章评论管理-编辑")
   @RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
   public Result<?> edit(@RequestBody CommentDO articleCommentDO) {
       articleCommentDOService.updateById(articleCommentDO);
       return Result.OK("编辑成功!");
   }

   /**
    * 通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "文章评论管理-通过id删除")
   @ApiOperation(value="文章评论管理-通过id删除", notes="文章评论管理-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       articleCommentDOService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    * 批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "文章评论管理-批量删除")
   @ApiOperation(value="文章评论管理-批量删除", notes="文章评论管理-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.articleCommentDOService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功！");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "文章评论管理-通过id查询")
   @ApiOperation(value="文章评论管理-通过id查询", notes="文章评论管理-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       CommentDO articleCommentDO = articleCommentDOService.getById(id);
       return Result.OK(articleCommentDO);
   }

 /**
  * 导出excel
  *
  * @param request
  * @param articleCommentDO
  */
 @RequestMapping(value = "/exportXls")
 public ModelAndView exportXls(HttpServletRequest request, CommentDO articleCommentDO) {
     return super.exportXls(request, articleCommentDO, CommentDO.class, "文章评论管理");
 }

 /**
  * 通过excel导入数据
  *
  * @param request
  * @param response
  * @return
  */
 @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
 public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
     return super.importExcel(request, response, CommentDO.class);
 }

}
