package org.bobo.modules.scloud.api;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.UserDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.dto.ViewHistoryDTO;
import org.bobo.service.IViewHistoryDOService;
import org.bobo.vo.ViewHistoryVO;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 播放历史记录管理
 * @Author: boBo
 * @Date: 2023-06-03
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "用户端——播放历史记录管理")
@RestController
@RequestMapping("/api/viewHistoryDO")
public class ViewHistoryDOController extends JeecgController<ViewHistoryDO, IViewHistoryDOService> {
    @Autowired
    private IViewHistoryDOService viewHistoryDOService;
    @Autowired
    private RedissonClient redissonClient;
    /**
     * 分页列表查询
     *
     * @param viewHistoryDTO
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "播放历史记录管理-分页列表查询")
    @ApiOperation(value = "播放历史记录管理-分页列表查询", notes = "播放历史记录管理-分页列表查询")
    @GetMapping(value = "/list")
    public Result<IPage<ViewHistoryVO>> queryPageList(ViewHistoryDTO viewHistoryDTO,
                                                      @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                      @RequestParam(name = "pageSize", defaultValue = "50") Integer pageSize,
                                                      HttpServletRequest req) {
        Page<ViewHistoryDO> page = new Page<ViewHistoryDO>(pageNo, pageSize);
        IPage<ViewHistoryVO> pageList = viewHistoryDOService.customPage(page, viewHistoryDTO);
        return Result.OK(pageList);
    }

    /**
     * 记录观看历史
     *
     * @param viewHistoryDTO
     * @return
     */
    @AutoLog(value = "播放历史记录管理-记录观看历史")
    @ApiOperation(value = "播放历史记录管理-记录观看历史", notes = "播放历史记录管理-记录观看历史")
    @PostMapping(value = "/recordHistory")
    public Result<?> recordHistory(@RequestBody @Validated ViewHistoryDTO viewHistoryDTO) throws Exception {
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();
        RLock rLock = redissonClient.getLock("recordHistory--" + user.getId());
        try {
            if (rLock.tryLock(15, TimeUnit.SECONDS)) {
                viewHistoryDOService.recordHistory(viewHistoryDTO);
            } else {
                throw new BusinessException(ExceptionEnums.TO_FAST);
            }
        } catch (Exception e) {
            throw new BusinessException(ExceptionEnums.TO_FAST);
        } finally {
            //释放锁
            rLock.unlock();
        }
        return Result.OK("操作成功！");
    }

    /**
     * 获取当前视频合集观看到哪个视频了
     *
     * @param videoSetId
     * @return
     */
    @AutoLog(value = "播放历史记录管理-获取当前视频合集观看到哪个视频了")
    @ApiOperation(value = "播放历史记录管理-获取当前视频合集观看到哪个视频了", notes = "播放历史记录管理-获取当前视频合集观看到哪个视频了")
    @PostMapping(value = "/getHistoryVideoByVideoSetId")
    public Result<?> getHistoryVideoByVideoSetId(@RequestParam("videoSetId") Long videoSetId) throws Exception {
        Long videoId = viewHistoryDOService.getHistoryVideoByVideoSetId(videoSetId);
        return Result.OK(videoId);
    }

    /**
     * videoId
     *
     * @param videoId
     * @return ViewHistoryVO
     */
    @AutoLog(value = "播放历史记录管理-查询历史播放时长")
    @ApiOperation(value = "播放历史记录管理-查询历史播放时长", notes = "播放历史记录管理-查询历史播放时长")
    @GetMapping(value = "/queryTime")
    public Result<Integer> queryById(@ApiParam("视频id") @RequestParam(name = "videoId", required = true) Long videoId) {
        Integer time = viewHistoryDOService.queryTime(videoId);
        return Result.OK(time);
    }

}
