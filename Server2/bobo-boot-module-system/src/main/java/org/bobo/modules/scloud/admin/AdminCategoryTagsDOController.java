package org.bobo.modules.scloud.admin;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.bobo.common.api.vo.Result;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.bean.CategoryTagsDO;
import org.bobo.common.util.oConvertUtils;
import org.bobo.dto.CategoryTagSetDTO;
import org.bobo.service.ICategoryTagsDOService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.common.system.base.controller.JeecgController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 视频分类标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags="总后台——视频分类标签管理")
@RestController
@RequestMapping("/admin/categoryTagsDO")
public class AdminCategoryTagsDOController extends JeecgController<CategoryTagsDO, ICategoryTagsDOService> {
	@Autowired
	private ICategoryTagsDOService categoryTagsDOService;
	
	/**
	 * 列表查询
	 *
	 * @param categoryTagsDO
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "视频分类标签管理-列表查询")
	@ApiOperation(value="视频分类标签管理-列表查询", notes="视频分类标签管理-列表查询")
	@GetMapping(value = "/listAll")
	public Result<?> listAll(CategoryTagsDO categoryTagsDO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		LambdaQueryWrapper<CategoryTagsDO> queryWrapper=new LambdaQueryWrapper<>();
		if(oConvertUtils.isNotEmpty(categoryTagsDO.getCategoryName())){
			queryWrapper.eq(CategoryTagsDO::getCategoryName,categoryTagsDO.getCategoryName());
		}
		List<CategoryTagsDO> list = categoryTagsDOService.list(queryWrapper);
		return Result.OK(list);
	}
	
	/**
	 * 设置视频分类标签
	 *
	 * @param categoryTagSetDTO
	 * @return
	 */
	@AutoLog(value = "视频分类标签管理-设置视频分类标签")
	@ApiOperation(value="视频分类标签管理-设置视频分类标签", notes="视频分类标签管理-设置视频分类标签")
	@PostMapping(value = "/setCategoryTag")
	public Result<?> set(@RequestBody CategoryTagSetDTO categoryTagSetDTO) {
		categoryTagsDOService.setCategoryTag(categoryTagSetDTO);
		return Result.OK("设置成功！");
	}
}
