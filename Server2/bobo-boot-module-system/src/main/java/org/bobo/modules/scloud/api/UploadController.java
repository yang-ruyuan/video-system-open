package org.bobo.modules.scloud.api;

import org.bobo.common.api.vo.Result;
import org.bobo.utils.Upload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private Upload upload;

    @PostMapping(value = "/file")
    public Result<?> uploadFile(@RequestParam("file") MultipartFile multipartFile) {
        return Result.OK(upload.uploadImg(multipartFile));
    }

    @PostMapping(value = "/uploadVideoToM3U8")
    public Result<?> uploadVideoToM3U8(@RequestParam("file") MultipartFile multipartFile) {
        return Result.OK(upload.uploadVideoToM3U8(multipartFile));
    }
}
