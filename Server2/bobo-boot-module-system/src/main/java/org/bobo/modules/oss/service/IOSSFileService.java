package org.bobo.modules.oss.service;

import java.io.IOException;

import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.modules.oss.entity.OSSFile;
import org.springframework.web.multipart.MultipartFile;

public interface IOSSFileService extends IService<OSSFile> {

	String uploadUser(MultipartFile multipartFile) throws IOException;

	String uploadAdmin(MultipartFile multipartFile) throws IOException;

	boolean delete(OSSFile ossFile);

}
