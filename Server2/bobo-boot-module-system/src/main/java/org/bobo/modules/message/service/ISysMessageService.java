package org.bobo.modules.message.service;

import org.bobo.common.system.base.service.JeecgService;
import org.bobo.modules.message.entity.SysMessage;

/**
 * @Description: 消息
 * @Author: jeecg-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageService extends JeecgService<SysMessage> {

}
