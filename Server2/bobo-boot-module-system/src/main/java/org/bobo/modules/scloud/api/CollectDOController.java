package org.bobo.modules.scloud.api;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.bean.CollectDO;
import org.bobo.bean.UserDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.dto.CollectDTO;
import org.bobo.service.ICollectDOService;
import org.bobo.vo.CollectVO;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 收藏管理
 * @Author: boBo
 * @Date: 2023-06-03
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "用户端——收藏管理")
@RestController
@RequestMapping("/api/collectDO")
public class CollectDOController extends JeecgController<CollectDO, ICollectDOService> {
    @Autowired
    private ICollectDOService collectDOService;

    /**
     * 分页列表查询
     *
     * @param collectDTO
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "收藏管理-分页列表查询")
    @ApiOperation(value = "收藏管理-分页列表查询", notes = "收藏管理-分页列表查询")
    @GetMapping(value = "/list")
    public Result<IPage<CollectVO>> queryPageList(CollectDTO collectDTO,
                                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                  @RequestParam(name = "pageSize", defaultValue = "50") Integer pageSize,
                                                  HttpServletRequest req) {
        Page<CollectDO> page = new Page<CollectDO>(pageNo, pageSize);
        IPage<CollectVO> pageList = collectDOService.customPage(page, collectDTO);

        return Result.OK(pageList);
    }

    /**
     * 收藏或取消收藏
     *
     * @param collectDTO
     * @return
     */
    @AutoLog(value = "收藏管理-收藏或取消收藏")
    @ApiOperation(value = "收藏管理-收藏或取消收藏", notes = "收藏管理-收藏或取消收藏")
    @PostMapping(value = "/saveOrDel")
    public Result<?> add(@RequestBody @Validated CollectDTO collectDTO) {
        return collectDOService.saveOrDel(collectDTO);
    }

    /**
     * 查看是否收藏
     *
     * @param videoId
     * @return
     */
    @AutoLog(value = "收藏管理-检查是否收藏")
    @ApiOperation(value = "收藏管理-检查是否收藏", notes = "收藏管理-检查是否收藏")
    @GetMapping(value = "/check")
    public Result<?> check(@RequestParam("videoId") Long videoId) {
        return collectDOService.check(videoId);
    }

    /**
     * 编辑
     *
     * @param collectDO
     * @return
     */
    @AutoLog(value = "收藏管理-编辑")
    @ApiOperation(value = "收藏管理-编辑", notes = "收藏管理-编辑")
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public Result<?> edit(@RequestBody CollectDO collectDO) {
        collectDOService.updateById(collectDO);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "收藏管理-通过id删除")
    @ApiOperation(value = "收藏管理-通过id删除", notes = "收藏管理-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        collectDOService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "收藏管理-批量删除")
    @ApiOperation(value = "收藏管理-批量删除", notes = "收藏管理-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.collectDOService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "收藏管理-通过id查询")
    @ApiOperation(value = "收藏管理-通过id查询", notes = "收藏管理-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        CollectDO collectDO = collectDOService.getById(id);
        return Result.OK(collectDO);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param collectDO
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CollectDO collectDO) {
        return super.exportXls(request, collectDO, CollectDO.class, "收藏管理");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CollectDO.class);
    }

}
