package org.bobo.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.sys.entity.SysTenant;

public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
