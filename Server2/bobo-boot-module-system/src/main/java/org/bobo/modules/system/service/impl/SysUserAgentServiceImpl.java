package org.bobo.modules.system.service.impl;

import org.bobo.modules.system.mapper.SysUserAgentMapper;
import org.bobo.modules.system.service.ISysUserAgentService;
import org.bobo.sys.entity.SysUserAgent;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户代理人设置
 * @Author: jeecg-boot
 * @Date:  2019-04-17
 * @Version: V1.0
 */
@Service
public class SysUserAgentServiceImpl extends ServiceImpl<SysUserAgentMapper, SysUserAgent> implements ISysUserAgentService {

}
