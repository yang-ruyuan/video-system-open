package org.bobo.modules.scloud.api;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.zhyd.oauth.utils.IpUtils;
import org.bobo.bean.UserDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.bean.VideoSetDO;
import org.bobo.common.util.IPUtils;
import org.bobo.common.util.bobo.IpUtil;
import org.bobo.dto.VideoSetDTO;
import org.bobo.service.IVideoSetDOService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.common.system.base.controller.JeecgController;

import org.bobo.vo.VideoSetVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date: 2023-04-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "用户端——视频合集管理")
@RestController
@RequestMapping("/api/videoSetDO")
public class VideoSetDOController extends JeecgController<VideoSetDO, IVideoSetDOService> {
    @Autowired
    private IVideoSetDOService videoSetDOService;

    /**
     * 分页列表查询
     *
     * @param videoSetDTO
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "视频合集管理-分页列表查询")
    @ApiOperation(value = "视频合集管理-分页列表查询", notes = "视频合集管理-分页列表查询")
    @GetMapping(value = "/list")
    public Result<IPage<VideoSetVO>> queryPageList(VideoSetDTO videoSetDTO,
                                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                   HttpServletRequest req) {
        Page<VideoSetDO> page = new Page<VideoSetDO>(pageNo, pageSize);
        IPage<VideoSetVO> pageList = videoSetDOService.customPage(page, videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 视频专辑全查询
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    @AutoLog(value = "视频合集管理-视频专辑全查询")
    @ApiOperation(value = "视频合集管理-视频专辑全查询", notes = "视频合集管理-视频专辑全查询")
    @GetMapping(value = "/videoSetAll")
    public Result<List<VideoSetVO>> videoSetAll(VideoSetDTO videoSetDTO) {
        List<VideoSetVO> pageList = videoSetDOService.customList(videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 今日热门查询
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    @AutoLog(value = "视频合集管理-今日热门查询")
    @ApiOperation(value = "视频合集管理-今日热门查询", notes = "视频合集管理-今日热门查询")
    @GetMapping(value = "/videoSetHostList")
    public Result<List<VideoSetVO>> videoSetHostList(VideoSetDTO videoSetDTO) {
        List<VideoSetVO> pageList = videoSetDOService.videoSetHostList(videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 本周热门查询
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    @AutoLog(value = "视频合集管理-本周热门查询")
    @ApiOperation(value = "视频合集管理-本周热门查询", notes = "视频合集管理-本周热门查询")
    @GetMapping(value = "/videoSetHostListByWeek")
    public Result<List<VideoSetVO>> videoSetHostListByWeek(VideoSetDTO videoSetDTO) {
        List<VideoSetVO> pageList = videoSetDOService.videoSetHostListByWeek(videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 推荐
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    @AutoLog(value = "视频合集管理-推荐")
    @ApiOperation(value = "视频合集管理-推荐", notes = "视频合集管理-推荐")
    @GetMapping(value = "/recommend")
    public Result<IPage<VideoSetVO>> recommend(VideoSetDTO videoSetDTO,
                                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<VideoSetDO> page = new Page<VideoSetDO>(pageNo, pageSize);
        IPage<VideoSetVO> pageList = videoSetDOService.recommend(page, videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 推荐
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    @AutoLog(value = "视频合集管理-推荐 带token请求这个")
    @ApiOperation(value = "视频合集管理-推荐 带token请求这个", notes = "视频合集管理-推荐 带token请求这个")
    @GetMapping(value = "/recommendAuth")
    public Result<IPage<VideoSetVO>> recommendAuth(VideoSetDTO videoSetDTO,
                                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<VideoSetDO> page = new Page<VideoSetDO>(pageNo, pageSize);
        IPage<VideoSetVO> pageList = videoSetDOService.recommendAuth(page, videoSetDTO);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param videoSetDO
     * @return
     */
    @AutoLog(value = "视频合集管理-添加")
    @ApiOperation(value = "视频合集管理-添加", notes = "视频合集管理-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody VideoSetDO videoSetDO) {
        videoSetDOService.save(videoSetDO);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param videoSetDO
     * @return
     */
    @AutoLog(value = "视频合集管理-编辑")
    @ApiOperation(value = "视频合集管理-编辑", notes = "视频合集管理-编辑")
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public Result<?> edit(@RequestBody VideoSetDO videoSetDO) {
        videoSetDOService.updateById(videoSetDO);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "视频合集管理-通过id删除")
    @ApiOperation(value = "视频合集管理-通过id删除", notes = "视频合集管理-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        videoSetDOService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "视频合集管理-批量删除")
    @ApiOperation(value = "视频合集管理-批量删除", notes = "视频合集管理-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.videoSetDOService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "视频合集管理-通过id查询")
    @ApiOperation(value = "视频合集管理-通过id查询", notes = "视频合集管理-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<VideoSetVO> queryById(@RequestParam(name = "id", required = true) String id) throws InterruptedException {
        VideoSetVO videoSetDO = videoSetDOService.customGetById(id);
        return Result.OK(videoSetDO);
    }
    /**
     * 点击量累积
     *
     * @param videoSetId
     * @return
     */
    @AutoLog(value = "视频合集管理-点击量累积")
    @ApiOperation(value = "视频合集管理-点击量累积", notes = "视频合集管理-点击量累积")
    @GetMapping(value = "/addClick")
    public Result<VideoSetVO> addClick(@RequestParam(name = "videoSetId", required = true) Long videoSetId,
                                       HttpServletRequest req) throws InterruptedException {
        String ipAddr = IPUtils.getIpAddr(req);
//        String ipAddr = IpUtil.getIpAddr(req);

        videoSetDOService.addClick(videoSetId,ipAddr);
        return Result.OK();
    }
    /**
     * 导出excel
     *
     * @param request
     * @param videoSetDO
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, VideoSetDO videoSetDO) {
        return super.exportXls(request, videoSetDO, VideoSetDO.class, "视频合集管理");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, VideoSetDO.class);
    }

}
