package org.bobo.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.sys.entity.SysDataSource;

/**
 * @Description: 多数据源管理
 * @Author: jeecg-boot
 * @Date: 2019-12-25
 * @Version: V1.0
 */
public interface ISysDataSourceService extends IService<SysDataSource> {

}
