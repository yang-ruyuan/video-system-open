package org.bobo.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bobo.modules.oss.entity.OSSFile;

public interface OSSFileMapper extends BaseMapper<OSSFile> {

}
