package org.bobo.modules.scloud.admin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bobo.common.api.vo.Result;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.common.system.query.QueryGenerator;
import org.bobo.common.aspect.annotation.AutoLog;
import org.bobo.bean.VideoSetDO;
import org.bobo.dto.VideoSetDTO;
import org.bobo.service.IVideoSetDOService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.bobo.common.system.base.controller.JeecgController;

import org.bobo.vo.VideoSetVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Slf4j
@Api(tags="总后台——视频合集管理")
@RestController
@RequestMapping("/admin/videoSetDO")
public class AdminVideoSetDOController extends JeecgController<VideoSetDO, IVideoSetDOService> {
	@Autowired
	private IVideoSetDOService videoSetDOService;
	
	/**
	 * 分页列表查询
	 *
	 * @param videoSetDO 参数
	 * @param pageNo 分页
	 * @param pageSize 分页
	 * @param req 请求
	 * @return
	 */
	@AutoLog(value = "视频合集管理-分页列表查询")
	@ApiOperation(value="视频合集管理-分页列表查询", notes="视频合集管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(VideoSetDO videoSetDO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<VideoSetDO> queryWrapper = QueryGenerator.initQueryWrapper(videoSetDO, req.getParameterMap());
		Page<VideoSetDO> page = new Page<VideoSetDO>(pageNo, pageSize);
		IPage<VideoSetDO> pageList = videoSetDOService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	 /**
	  * 视频专辑全查询
	  *
	  * @param videoSetDTO 参数
	  * @return List<VideoSetDO>
	  */
	 @AutoLog(value = "视频合集管理-视频专辑全查询")
	 @ApiOperation(value="视频合集管理-视频专辑全查询", notes="视频合集管理-视频专辑全查询")
	 @GetMapping(value = "/videoSetAll")
	 public Result<List<VideoSetVO>> videoSetAll(VideoSetDTO videoSetDTO) {
		 List<VideoSetVO> pageList = videoSetDOService.customList(videoSetDTO);
		 return Result.OK(pageList);
	 }

	/**
	 * 添加
	 *
	 * @param videoSetDO 参数
	 * @return
	 */
	@AutoLog(value = "视频合集管理-添加")
	@ApiOperation(value="视频合集管理-添加", notes="视频合集管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody VideoSetDO videoSetDO) {
		try {
			videoSetDOService.save(videoSetDO);
		}catch (DuplicateKeyException e){
			throw new BusinessException(ExceptionEnums.VIDEO_SET_IS_EXIST);
		}
		return Result.OK("添加成功！");
	}



	 /**
	 * 编辑
	 *
	 * @param videoSetDO
	 * @return
	 */
	@AutoLog(value = "视频合集管理-编辑")
	@ApiOperation(value="视频合集管理-编辑", notes="视频合集管理-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody VideoSetDO videoSetDO) {
		try {
			videoSetDOService.updateById(videoSetDO);
		}catch (DuplicateKeyException e){
			throw new BusinessException(ExceptionEnums.VIDEO_SET_IS_EXIST);
		}
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频合集管理-通过id删除")
	@ApiOperation(value="视频合集管理-通过id删除", notes="视频合集管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		videoSetDOService.removeById(id);
		return Result.OK("删除成功!");
	}
	 /**
	  * 设置为精选合集
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "视频合集管理-设置为精选合集")
	 @ApiOperation(value="视频合集管理-设置为精选合集", notes="视频合集管理-设置为精选合集")
	 @GetMapping(value = "/carefullyChosen")
	 public Result<?> carefullyChosen(@RequestParam(name="id",required=true) String id) {
		 videoSetDOService.carefullyChosen(id);
		 return Result.OK("操作成功!");
	 }
	 /**
	  * 取消精选
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "视频合集管理-取消精选")
	 @ApiOperation(value="视频合集管理-取消精选", notes="视频合集管理-取消精选")
	 @GetMapping(value = "/unCarefullyChosen")
	 public Result<?> unCarefullyChosen(@RequestParam(name="id",required=true) String id) {
		 videoSetDOService.unCarefullyChosen(id);
		 return Result.OK("操作成功!");
	 }
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "视频合集管理-批量删除")
	@ApiOperation(value="视频合集管理-批量删除", notes="视频合集管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.videoSetDOService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "视频合集管理-通过id查询")
	@ApiOperation(value="视频合集管理-通过id查询", notes="视频合集管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		VideoSetDO videoSetDO = videoSetDOService.getById(id);
		return Result.OK(videoSetDO);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param videoSetDO
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, VideoSetDO videoSetDO) {
      return super.exportXls(request, videoSetDO, VideoSetDO.class, "视频合集管理");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, VideoSetDO.class);
  }

}
