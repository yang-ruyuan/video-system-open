package org.bobo.modules.scloud.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.CollectDO;
import org.bobo.bean.CommentDO;
import org.bobo.bean.UserDO;

import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.common.util.IPUtils;
import org.bobo.common.util.oConvertUtils;
import org.bobo.dto.CommentDTO;
import org.bobo.service.ICommentDOService;
import org.bobo.common.api.vo.Result;
import org.bobo.common.system.base.controller.JeecgController;
import org.bobo.common.aspect.annotation.AutoLog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;


import org.bobo.vo.CommentVO;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 文章评论管理
 * @Author: jeecg-boot
 * @Date: 2022-02-27
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "用户端——文章评论管理")
@RestController
@RequestMapping("/api/comment")
public class CommentDOController extends JeecgController<CommentDO, ICommentDOService> {
    @Autowired
    private ICommentDOService articleCommentDOService;
    @Autowired
    private RedissonClient redissonClient;
    /**
     * @param type
     * @param sourceId
     * @param parentId
     * @return
     */
    @ApiOperation(value = "客户端文章评论-分页列表查询", notes = "客户端文章评论-分页列表查询")
    @GetMapping(value = "/list")
    public Result<IPage<CommentVO>> queryPageList(@ApiParam("评论类型 video gags") @RequestParam("type") String type,
                                                  @ApiParam("视频源id") @RequestParam("sourceId") Long sourceId,
                                                  @ApiParam("sort 热门 最新") @RequestParam("sort") String sort,
                                                  @ApiParam("父评论id") @RequestParam(value = "parentId", required = false) Long parentId,
                                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                  @RequestParam(name = "pageSize", defaultValue = "50") Integer pageSize,
                                                  HttpServletRequest req) {
        Page<CommentDO> page = new Page<CommentDO>(pageNo, pageSize);
        IPage<CommentVO> iPage = articleCommentDOService.customPage(page, type, sourceId, parentId,sort);
        return Result.ok(iPage);
    }

    /**
     * @param type
     * @param sourceId
     * @param parentId
     * @return
     */
    @ApiOperation(value = "客户端文章评论-分页列表查询不需要token", notes = "客户端文章评论-分页列表查询不需要token")
    @GetMapping(value = "/listUnAuth")
    public Result<IPage<CommentVO>> listUnAuth(@ApiParam("评论类型 video gags") @RequestParam("type") String type,
                                               @ApiParam("视频源id") @RequestParam("sourceId") Long sourceId,
                                               @ApiParam("sort 热门 最新") @RequestParam("sort") String sort,
                                               @ApiParam("父评论id") @RequestParam(value = "parentId", required = false) Long parentId,
                                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                               @RequestParam(name = "pageSize", defaultValue = "50") Integer pageSize,
                                               HttpServletRequest req) {
        Page<CommentDO> page = new Page<CommentDO>(pageNo, pageSize);
        IPage<CommentVO> iPage = articleCommentDOService.customPageUnAuth(page, type, sourceId, parentId,sort);
        return Result.ok(iPage);
    }

    /**
     * 评论
     *
     * @param commentDTO
     * @return
     */
    @ApiOperation(value = "客户端文章评论-添加", notes = "客户端文章评论-添加")
    @PostMapping(value = "/add")
    public Result<CommentVO> add(@RequestBody @Validated CommentDTO commentDTO,HttpServletRequest request) {
        String ipAddr = IPUtils.getIpAddr(request);
        commentDTO.setIp(ipAddr);
        CommentVO comment = articleCommentDOService.comment(commentDTO);
        return Result.ok(comment);
    }
    /**
     * 评论数量统计
     *
     * @param type
     * @param sourceId
     * @return
     */
    @ApiOperation(value = "客户端文章评论-评论数量统计", notes = "客户端文章评论-评论数量统计")
    @PostMapping(value = "/commentTotal")
    public Result<Long> commentTotal(@RequestParam("type") String type,@RequestParam("sourceId") Long sourceId) {
        return Result.ok(articleCommentDOService.commentTotal(sourceId,type));
    }

    /**
     * 点赞
     *
     * @param commentId
     * @return
     */
    @ApiOperation(value = "客户端文章评论-点赞", notes = "客户端文章评论-点赞")
    @PostMapping(value = "/like")
    public Result like(@RequestParam("commentId") Long commentId) {
        //有线程安全问题 从这里开始加锁
        RLock rLock = redissonClient.getLock("commentId_like--"+commentId);
        try {
            if (rLock.tryLock(15, TimeUnit.SECONDS)) {
                articleCommentDOService.like(commentId);
            } else {
                throw new BusinessException(ExceptionEnums.TO_FAST);
            }

        } catch (Exception e) {
            throw new BusinessException(500,e.getMessage());
        } finally {
            //释放锁
            rLock.unlock();
        }

        return Result.ok();
    }
    /**
     * 编辑
     *
     * @param articleCommentDO
     * @return
     */
    @AutoLog(value = "文章评论管理-编辑")
    @ApiOperation(value = "文章评论管理-编辑", notes = "文章评论管理-编辑")
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public Result<?> edit(@RequestBody CommentDO articleCommentDO) {
        articleCommentDOService.updateById(articleCommentDO);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "文章评论管理-通过id删除")
    @ApiOperation(value = "文章评论管理-通过id删除", notes = "文章评论管理-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        articleCommentDOService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "文章评论管理-批量删除")
    @ApiOperation(value = "文章评论管理-批量删除", notes = "文章评论管理-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.articleCommentDOService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "文章评论管理-通过id查询")
    @ApiOperation(value = "文章评论管理-通过id查询", notes = "文章评论管理-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        CommentDO articleCommentDO = articleCommentDOService.getById(id);
        return Result.OK(articleCommentDO);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param articleCommentDO
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CommentDO articleCommentDO) {
        return super.exportXls(request, articleCommentDO, CommentDO.class, "文章评论管理");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CommentDO.class);
    }

}
