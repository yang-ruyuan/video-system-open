package org.bobo.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class RandomUtils {

    /**
     * 获取指定长度随机数
     *
     * @param len
     * @return
     */
    public static String getRandom(int len) {
        Random r = new Random();
        int bitNum = 1;
        for (int i = 0; i < len; i++) {
            bitNum = bitNum * 10;
        }
        long num = Math.abs(r.nextLong() % (bitNum));
        String s = String.valueOf(num);
        for (int i = len - s.length(); i > 0; i--) {
            s = "0" + s;
        }
        if (s.length() > len) {
            s = s.substring(0, len);
        }
        return s;
    }


    public static String  randOrderNo() {

        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());

        String seconds = new SimpleDateFormat("HHmmss").format(new Date());

        return date+"00001000"+getTwo()+"00"+seconds+getTwo();



    }

    /**

     * 产生随机的2位数

     * @return

     */

    public static String getTwo(){

        Random rad=new Random();

        String result = rad.nextInt(100) +"";

        if(result.length()==1){

                result = "0" + result;

        }

        return result;

    }


}
