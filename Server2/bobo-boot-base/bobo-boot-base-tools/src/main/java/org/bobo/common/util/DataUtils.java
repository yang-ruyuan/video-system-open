package org.bobo.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DataUtils {
    /**
     * 时间转字符串
     *
     * @param format  格式
     * @param time 日期
     * @return
     */
    public static String dateToString(String format, Date time) {
        SimpleDateFormat sdf_input = new SimpleDateFormat(format);//输入格式
        return sdf_input.format(time);
    }


}
