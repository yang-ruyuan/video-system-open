package org.bobo.common.constant.enums;

public enum MyConstant {
    LOGOTYPE_ADMIN("总后台登陆"),
    LOGOTYPE_USER("用户端登陆"),
    ;

    private String value;

    MyConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
