package org.bobo.common.constant.enums;

public enum BaseEnums {
    LIMIT("limit","limit"),

    IS_CAREFULLY_CHOSEN("1","是精选视频"),

    UN_CAREFULLY_CHOSEN("0","不是精选视频"),
    ;

    private String value;

    private String tips;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    BaseEnums(String value, String tips) {
        this.value = value;
        this.tips = tips;
    }
}
