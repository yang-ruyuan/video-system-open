package org.bobo.common.exception;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.springframework.context.annotation.Configuration;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Configuration
public class BusinessException extends RuntimeException {

    private int code;
    private String msg;

    public BusinessException(ExceptionEnums videoSetNotFind) {
        this.code=videoSetNotFind.getCode();
        this.msg=videoSetNotFind.getMsg();
    }
}
