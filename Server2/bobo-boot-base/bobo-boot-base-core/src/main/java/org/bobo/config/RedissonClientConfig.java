package org.bobo.config;


import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonClientConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password}")
    private String password;

    @Bean
    public RedissonClient getRedisson() {
        Config config = new Config();
        //解决如果password为空会导致redisson连不上的问题
        if (password.equals("")) {
            config.useSingleServer()
                    .setAddress("redis://" + host + ":" + port + "")
                    .setRetryInterval(50000)
                    .setTimeout(100000)
                    .setConnectTimeout(100000);
        } else {
            config.useSingleServer()
                    .setAddress("redis://" + host + ":" + port + "").setPassword(password)
                    .setRetryInterval(50000)
                    .setTimeout(100000)
                    .setConnectTimeout(100000);
        }

        return Redisson.create(config);
    }
}
