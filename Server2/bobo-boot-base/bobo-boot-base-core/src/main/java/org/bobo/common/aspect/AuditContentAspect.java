package org.bobo.common.aspect;

import com.google.common.collect.Lists;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.bobo.common.aspect.annotation.AuditContent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Aspect
@Component
public class AuditContentAspect {
    @Pointcut("@annotation(org.bobo.common.aspect.annotation.AuditContent)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {

        Object[] args = point.getArgs();
        for(Object arg:args){
                List<Field> fieldList=new ArrayList<>(Arrays.asList(arg.getClass().getDeclaredFields()));
            for (Field field : fieldList) {
            if (field.getAnnotation(AuditContent.class) != null) {  //判断字段是否包含Hit注解
                System.out.println(field);
            }
        }
        }

        Object result = point.proceed();
//        List<Field> fieldList = new ArrayList<>();
//        while (clazz != null) {
//            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
//            clazz = clazz.getSuperclass();
//        }
//        Field[] fields = new Field[fieldList.size()];
//        fieldList.toArray(fields);
//        for (Field field : fieldList) {
//            if (field.getAnnotation(AuditContent.class) != null) {  //判断字段是否包含Hit注解
//                System.out.println(field);
//                //                String code = field.getAnnotation(AuditContent.class).hitCode(); //获取字段注解的hitCode值
////                String key = String.valueOf(item.get(field.getName()));
////                String textValue = translateDictValue(code, key);
//            }
//        }
        return result;
    }

//    public static void reflect(Object obj) {
//
//        if (obj == null)
//
//            return;
//
//        Field[] fields = obj.getClass().getDeclaredFields();
//
//        String[] types1={"int","java.lang.String","boolean","char","float","double","long","short","byte"};
//
//        String[] types2={"Integer","java.lang.String","java.lang.Boolean","java.lang.Character","java.lang.Float","java.lang.Double","java.lang.Long","java.lang.Short","java.lang.Byte"};
//
//        for (int j = 0; j < fields.length; j++) {
//
//            fields[j].setAccessible(true);
//// 字段名
//            System.out.print(fields[j].getName() + ":");
//// 字段值
//            for(int i=0;i<fields.length;i++){
//            if(fields[j].getType().getName()
//                    .equalsIgnoreCase(types1[i])|| fields[j].getType().getName().equalsIgnoreCase(types2[i])){undefined
//                try {
//                    System.out.print(fields[j].get(obj)+" ");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//        }
//
//    }
//
//}



    /**
     * 获取方法的请求参数
     */
    private List<Object> getMethodArgs(ProceedingJoinPoint proceedingJoinPoint) {
        List<Object> methodArgs = Lists.newArrayList();
        for (Object arg : proceedingJoinPoint.getArgs()) {
            if (null != arg) {
                methodArgs.add(arg);
            }
        }
        return methodArgs;
    }


}
