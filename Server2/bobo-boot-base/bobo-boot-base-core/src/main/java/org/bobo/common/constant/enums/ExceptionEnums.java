package org.bobo.common.constant.enums;

public enum ExceptionEnums {
    SYSTEM_ERROR(500,"系统错误"),
    PARAM_ILLEGAL(500,"非法参数"),

    VIDEO_SET_NOT_FIND(500,"视频合集不存在，请重试"),
    VIDEO_SET_IS_EXIST(500,"视频合集已存在"),
    USER_IS_EXIST(500,"用户已经存在"),
    SMS_IS_EXIST(500,"验证码不存在或者已经过期"),
    SMS_IS_ERROR(500,"验证码错误"),

    TO_FAST(500,"客官，麻烦慢一点"),
    ;

    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int value) {
        this.code = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String tips) {
        this.msg = tips;
    }

    ExceptionEnums(int code, String tips) {
        this.code = code;
        this.msg = tips;
    }
}
