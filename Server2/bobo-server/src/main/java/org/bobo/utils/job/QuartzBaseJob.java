package org.bobo.utils.job;


import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class QuartzBaseJob {


    @Scheduled(cron = "0 0 2 * * ?")
    @Transactional
    public void configureTasks() {

        }
}
