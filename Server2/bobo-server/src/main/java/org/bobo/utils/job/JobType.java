package org.bobo.utils.job;


public enum JobType {

    ORDER_ADD("ORDER_ADD","新增订单定时任务"),

    ;

    JobType(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;

    private String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

