package org.bobo.serviceVO.impl;


import org.bobo.mapperVO.HotspotVOMapper;
import org.bobo.serviceVO.IHotspotVOService;
import org.bobo.vo.HotspotVO;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 热点表/热搜表 视图
 * @Author: boBo
 * @Date:   2022-03-26
 * @Version: V1.0
 */
@Service
public class HotspotVOServiceImpl extends ServiceImpl<HotspotVOMapper, HotspotVO> implements IHotspotVOService {

}
