package org.bobo.serviceVO;


import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.vo.SpuVO;

/**
 * @Description: 商品视图
 * @Author: boBo
 * @Date:   2022-04-05
 * @Version: V1.0
 */
public interface ISpuVOService extends IService<SpuVO> {

}
