package org.bobo.serviceVO.impl;


import org.bobo.mapperVO.SpuVOMapper;
import org.bobo.vo.SpuVO;
import org.bobo.serviceVO.ISpuVOService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 商品视图
 * @Author: boBo
 * @Date:   2022-04-05
 * @Version: V1.0
 */
@Service
public class SpuVOServiceImpl extends ServiceImpl<SpuVOMapper, SpuVO> implements ISpuVOService {

}
