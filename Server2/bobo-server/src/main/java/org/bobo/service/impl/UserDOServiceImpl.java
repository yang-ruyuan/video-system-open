package org.bobo.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.bobo.bean.UserDO;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.common.util.oConvertUtils;
import org.bobo.dto.UserDTO;
import org.bobo.mapper.UserDOMapper;
import org.bobo.service.IUserDOService;
import org.springframework.stereotype.Service;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
@Service
public class UserDOServiceImpl extends ServiceImpl<UserDOMapper, UserDO> implements IUserDOService {

    @Override
    public UserDO findByUserNam(String username) {
        return baseMapper.selectOne(new QueryWrapper<UserDO>().eq("user_name",username));
    }

    @Override
    public void customUpdate(UserDTO userDTO) {
        if(oConvertUtils.isEmpty(userDTO.getId())){
            throw new BusinessException(ExceptionEnums.SYSTEM_ERROR);
        }

        UserDO userDO=new UserDO();

        BeanUtil.copyProperties(userDTO,userDO);

         baseMapper.updateById(userDO);
    }
}
