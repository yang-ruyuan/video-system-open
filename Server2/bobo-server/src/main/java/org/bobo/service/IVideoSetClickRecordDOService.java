package org.bobo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.VideoSetClickRecordDO;

/**
 * @Description: 专辑点击记录
 * @Author: boBo
 * @Date:   2023-07-23
 * @Version: V1.0
 */
public interface IVideoSetClickRecordDOService extends IService<VideoSetClickRecordDO> {

}
