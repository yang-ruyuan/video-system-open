package org.bobo.service.impl;


import org.bobo.bean.VideoSetClickRecordDO;
import org.bobo.mapper.VideoSetClickRecordDOMapper;
import org.bobo.service.IVideoSetClickRecordDOService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专辑点击记录
 * @Author: boBo
 * @Date:   2023-07-23
 * @Version: V1.0
 */
@Service
public class VideoSetClickRecordDOServiceImpl extends ServiceImpl<VideoSetClickRecordDOMapper, VideoSetClickRecordDO> implements IVideoSetClickRecordDOService {

}
