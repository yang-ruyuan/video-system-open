package org.bobo.service.impl;

import org.bobo.bean.CategoryDO;
import org.bobo.mapper.CategoryDOMapper;
import org.bobo.service.ICategoryDOService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 视频分类管理/侧边栏管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Service
public class CategoryDOServiceImpl extends ServiceImpl<CategoryDOMapper, CategoryDO> implements ICategoryDOService {

}
