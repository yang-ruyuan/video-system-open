package org.bobo.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.ChasingDramaDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.dto.ChasingDramaDTO;
import org.bobo.vo.ChasingDramaVO;
import org.bobo.vo.ViewHistoryVO;

/**
 * @Description: 追剧管理管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface IChasingDramaDOService extends IService<ChasingDramaDO> {
    /**
     * 追剧或者取消追剧
     * @param videoSetId
     */
    void saveOrDel(Long videoSetId);

    ChasingDramaDO getByUserIdCommentId(Integer setId, Integer userId);

    /**
     * 查询追剧列表
     * @param page
     * @param chasingDramaDTO
     * @return
     */
    IPage<ChasingDramaVO> customPage(Page<ChasingDramaDO> page, ChasingDramaDTO chasingDramaDTO);
}
