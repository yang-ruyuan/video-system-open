package org.bobo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.bobo.bean.VideoDO;
import org.bobo.dto.VideoDTO;
import org.bobo.mapper.VideoDOMapper;
import org.bobo.service.IVideoDOService;
import org.bobo.vo.VideoVO;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 视频管理
 * @Author: boBo
 * @Date: 2023-04-09
 * @Version: V1.0
 */
@Service
public class VideoDOServiceImpl extends ServiceImpl<VideoDOMapper, VideoDO> implements IVideoDOService {
    /**
     * 自定义分页查询
     *
     * @param page
     * @param videoDTO
     * @return IPage<VideoVO>
     */
    @Override
    public IPage<VideoVO> customPage(Page<VideoDO> page, VideoDTO videoDTO) {
        LambdaQueryWrapper<VideoDO> queryWrapper = videoDTO.initQueryWrapper(videoDTO);
        return baseMapper.customPage(page, queryWrapper);
    }

    /**
     * 自定义全查询
     *
     * @param videoDTO
     * @return List<VideoVO>
     */
    @Override
    public List<VideoVO> customList(VideoDTO videoDTO) {
        LambdaQueryWrapper<VideoDO> queryWrapper = videoDTO.initQueryWrapper(videoDTO);
        return baseMapper.customList(queryWrapper);
    }

    /**
     * 添加
     *
     * @param videoDTO
     * @return
     */
    @Override
    public void saveDTO(VideoDTO videoDTO) {
        VideoDO videoDO = new VideoDO();
        BeanUtil.copyProperties(videoDTO, videoDO);
        baseMapper.insert(videoDO);
    }
}
