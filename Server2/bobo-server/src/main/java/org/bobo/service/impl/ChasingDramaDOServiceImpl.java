package org.bobo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.ChasingDramaDO;
import org.bobo.bean.CollectDO;
import org.bobo.bean.UserDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.constant.enums.BaseEnums;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.dto.ChasingDramaDTO;
import org.bobo.mapper.ChasingDramaDOMapper;
import org.bobo.service.IChasingDramaDOService;
import org.bobo.vo.ChasingDramaVO;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.concurrent.TimeUnit;

/**
 * @Description: 追剧管理管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Service
public class ChasingDramaDOServiceImpl extends ServiceImpl<ChasingDramaDOMapper, ChasingDramaDO> implements IChasingDramaDOService {
    @Autowired
    private RedissonClient redissonClient;
    /**
     * 追剧或者取消追剧
     * @param videoSetId
     */
    @Override
    public void saveOrDel(Long videoSetId) {
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();
        //从这里开始加锁
        RLock rLock = redissonClient.getLock("drama--"+user.getId());
        try {
            if (rLock.tryLock(15, TimeUnit.SECONDS)) {
                ChasingDramaDO chasingDramaDB = baseMapper.selectOne(new LambdaQueryWrapper<ChasingDramaDO>().eq(ChasingDramaDO::getUserId, user.getId()).eq(ChasingDramaDO::getVideoSetId, videoSetId).last(BaseEnums.LIMIT + " 1"));
                ChasingDramaDO chasingDramaDO = new ChasingDramaDO();
                if (chasingDramaDB == null) {
                    chasingDramaDO.setUserId(user.getId());
                    chasingDramaDO.setVideoSetId(videoSetId);
                    baseMapper.insert(chasingDramaDO);
                } else {
                    baseMapper.deleteById(chasingDramaDB.getId());
                }
            } else {
                throw new BusinessException(ExceptionEnums.TO_FAST);
            }
        } catch (Exception e) {
            throw new BusinessException(ExceptionEnums.TO_FAST);
        } finally {
            //释放锁
            rLock.unlock();
        }
    }

    /**
     * 根据用户id和集合id获取记录
     * @param setId
     * @param userId
     * @return
     */
    @Override
    public ChasingDramaDO getByUserIdCommentId(Integer setId, Integer userId) {
        return baseMapper.getByUserIdCommentId(setId,userId);
    }

    @Override
    public IPage<ChasingDramaVO> customPage(Page<ChasingDramaDO> page, ChasingDramaDTO chasingDramaDTO) {
        QueryWrapper<ChasingDramaDO> queryWrapper=chasingDramaDTO.initQueryWrapper(chasingDramaDTO);
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();
        queryWrapper.eq("cd.user_id", user.getId());
        return baseMapper.customPage(page,queryWrapper);
    }
}
