package org.bobo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.bobo.bean.CategoryTagsDO;
import org.bobo.dto.CategoryTagSetDTO;
import org.bobo.mapper.CategoryTagsDOMapper;
import org.bobo.service.ICategoryTagsDOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 视频分类标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Service
public class CategoryTagsDOServiceImpl extends ServiceImpl<CategoryTagsDOMapper, CategoryTagsDO> implements ICategoryTagsDOService {
    @Lazy
    @Autowired
    private ICategoryTagsDOService categoryTagsDOService;
    /**
     * 设置视频分类标签
     * @param categoryTagSetDTO
     */
    @Transactional
    @Override
    public void setCategoryTag(CategoryTagSetDTO categoryTagSetDTO) {
        baseMapper.delete(new LambdaQueryWrapper<CategoryTagsDO>().eq(CategoryTagsDO::getCategoryName, categoryTagSetDTO.getCategoryName()));

        categoryTagsDOService.saveBatch(categoryTagSetDTO.dtoTODOS(categoryTagSetDTO));
    }
}
