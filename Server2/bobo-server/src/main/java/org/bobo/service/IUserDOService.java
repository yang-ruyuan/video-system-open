package org.bobo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.UserDO;
import org.bobo.dto.UserDTO;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
public interface IUserDOService extends IService<UserDO> {

    UserDO findByUserNam(String username);
    /**
     * 修改用户信息
     *
     * @param userDTO
     * @return
     */
    void customUpdate(UserDTO userDTO);
}
