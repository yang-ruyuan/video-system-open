package org.bobo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.bobo.bean.VideoDO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.dto.VideoDTO;
import org.bobo.vo.VideoVO;

import java.util.List;

/**
 * @Description: 视频管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface IVideoDOService extends IService<VideoDO> {
    /**
     * 自定义分页查询
     * @param page
     * @param videoDTO
     * @return IPage<VideoVO>
     */
    IPage<VideoVO> customPage(Page<VideoDO> page, VideoDTO videoDTO);

    /**
     * 自定义列表查询
     * @param videoDTO
     * @return List<VideoVO>
     */
    List<VideoVO> customList(VideoDTO videoDTO);
    /**
     * 添加
     *
     * @param videoDTO
     * @return
     */
    void saveDTO(VideoDTO videoDTO);
}
