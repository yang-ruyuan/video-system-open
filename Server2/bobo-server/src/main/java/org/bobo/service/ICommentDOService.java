package org.bobo.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.CollectDO;
import org.bobo.bean.CommentDO;
import org.bobo.dto.CommentDTO;
import org.bobo.vo.CommentVO;

import java.util.List;

/**
 * @Description: 文章评论管理
 * @Author: bobo
 * @Date:   2022-02-27
 * @Version: V1.0
 */
public interface ICommentDOService extends IService<CommentDO> {


    /**
     *   评论
     * @param commentDTO
     * @return
     */
    CommentVO comment(CommentDTO commentDTO);

    /**
     * 自定义分页查询
     * @param page 分页
     * @param type 类型 video gags
     * @param sourceId 视频源id
     * @param parentId 父评论id
     * @param sort 排序 最新 最热
     * @return
     */
    IPage<CommentVO> customPage(Page<CommentDO> page, String type, Long sourceId, Long parentId,String sort);
    /**
     * 自定义分页查询 不需要认证
     * @param page 分页
     * @param type 类型 video gags
     * @param sourceId 视频源id
     * @param parentId 父评论id
     * @param sort 排序 最新 最热
     * @return
     */
    IPage<CommentVO> customPageUnAuth(Page<CommentDO> page, String type, Long sourceId, Long parentId,String sort);
    /**
     * 点赞
     *
     * @param commentId
     * @return
     */
    void like(Long commentId);
    /**
     * 评论数量统计
     *
     * @param type
     * @param sourceId
     * @return
     */
    Long commentTotal(Long sourceId, String type);
}
