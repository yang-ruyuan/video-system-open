package org.bobo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.bobo.bean.CommentLikeRecordDO;
import org.bobo.mapper.CommentLikeRecordDOMapper;
import org.bobo.service.ICommentLikeRecordDOService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 评论点赞记录
 * @Author: boBo
 * @Date:   2023-06-17
 * @Version: V1.0
 */
@Service
public class CommentLikeRecordDOServiceImpl extends ServiceImpl<CommentLikeRecordDOMapper, CommentLikeRecordDO> implements ICommentLikeRecordDOService {

    @Override
    public CommentLikeRecordDO getByUserIdCommentId(Long commentId, Integer userId) {
        return baseMapper.selectOne(new LambdaQueryWrapper<CommentLikeRecordDO>().eq(CommentLikeRecordDO::getUserId,userId).eq(CommentLikeRecordDO::getCommentId,commentId));
    }
}
