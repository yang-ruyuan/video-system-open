package org.bobo.service.impl;

import org.bobo.bean.VideoSetTagsDO;
import org.bobo.mapper.VideoSetTagsDOMapper;
import org.bobo.service.IVideoSetTagsDOService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 视频合集标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Service
public class VideoSetTagsDOServiceImpl extends ServiceImpl<VideoSetTagsDOMapper, VideoSetTagsDO> implements IVideoSetTagsDOService {

}
