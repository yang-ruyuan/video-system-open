package org.bobo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.CollectDO;
import org.bobo.bean.UserDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.common.api.vo.Result;
import org.bobo.common.constant.enums.BaseEnums;
import org.bobo.common.constant.enums.ExceptionEnums;
import org.bobo.common.exception.BusinessException;
import org.bobo.dto.CollectDTO;
import org.bobo.mapper.CollectDOMapper;
import org.bobo.service.ICollectDOService;
import org.bobo.service.IViewHistoryDOService;
import org.bobo.vo.CollectVO;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.concurrent.TimeUnit;

/**
 * @Description: 收藏管理
 * @Author: boBo
 * @Date: 2023-06-03
 * @Version: V1.0
 */
@Service
public class CollectDOServiceImpl extends ServiceImpl<CollectDOMapper, CollectDO> implements ICollectDOService {
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private IViewHistoryDOService viewHistoryDOService;

    /**
     * 收藏或取消收藏
     *
     * @param collectDTO
     * @return
     */
    @Override
    public Result<?> saveOrDel(CollectDTO collectDTO) {
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();
        //从这里开始加锁
        RLock rLock = redissonClient.getLock("collect--"+user.getId());
        try {
            if (rLock.tryLock(15, TimeUnit.SECONDS)) {
                CollectDO collectDB = baseMapper.selectOne(new LambdaQueryWrapper<CollectDO>().eq(CollectDO::getUserId, user.getId()).eq(CollectDO::getVideoId, collectDTO.getVideoId()).last(BaseEnums.LIMIT + " 1"));
                CollectDO collectDO = new CollectDO();
                if (collectDB == null) {
                    collectDO.setUserId(user.getId());
                    collectDO.setTime(collectDTO.getTime());
                    collectDO.setVideoId(collectDTO.getVideoId());
                    collectDO.setSchedule(viewHistoryDOService.countSchedule(collectDTO.getVideoId(), collectDTO.getTime()));
                    baseMapper.insert(collectDO);
                    return Result.OK("收藏成功");
                } else {
                    baseMapper.deleteById(collectDB.getId());
                    return Result.OK("取消收藏成功");
                }
            } else {
                throw new BusinessException(ExceptionEnums.TO_FAST);
            }

        } catch (Exception e) {
            throw new BusinessException(ExceptionEnums.TO_FAST);
        } finally {
            //释放锁
            rLock.unlock();
        }
    }
    /**
     * 查看是否收藏
     *
     * @param videoId
     * @return
     */
    @Override
    public Result<?> check(Long videoId) {
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();
        CollectDO collectDB = baseMapper.selectOne(new LambdaQueryWrapper<CollectDO>().eq(CollectDO::getUserId, user.getId()).eq(CollectDO::getVideoId, videoId).last(BaseEnums.LIMIT + " 1"));
        if(collectDB!=null){
            return Result.OK(true);
        }else{
            return Result.OK(false);
        }
    }
    /**
     * 分页列表查询
     *
     * @param collectDTO
     * @return
     */
    @Override
    public IPage<CollectVO> customPage(Page<CollectDO> page, CollectDTO collectDTO) {
        QueryWrapper<CollectDO> queryWrapper=collectDTO.initQueryWrapper(collectDTO);
        return baseMapper.customPage(page,queryWrapper);
    }
}
