package org.bobo.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.bobo.bean.GagsDO;
import org.bobo.bean.VideoDO;
import org.bobo.dto.GagsDTO;
import org.bobo.mapper.GagsDOMapper;
import org.bobo.service.IGagsDOService;
import org.bobo.vo.GagsVO;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 花絮管理
 * @Author: boBo
 * @Date:   2023-06-07
 * @Version: V1.0
 */
@Service
public class GagsDOServiceImpl extends ServiceImpl<GagsDOMapper, GagsDO> implements IGagsDOService {

    @Override
    public void saveDTO(GagsDTO gagsDTO) {
        GagsDO gagsDO = new GagsDO();
        BeanUtil.copyProperties(gagsDTO, gagsDO);
        baseMapper.insert(gagsDO);
    }

    @Override
    public IPage<GagsVO> customPage(Page<GagsDO> page, GagsDTO gagsDTO) {
        LambdaQueryWrapper<GagsDO> lambdaQueryWrapper=gagsDTO.initQueryWrapper(gagsDTO);
        return baseMapper.customPage(page,lambdaQueryWrapper);
    }
}
