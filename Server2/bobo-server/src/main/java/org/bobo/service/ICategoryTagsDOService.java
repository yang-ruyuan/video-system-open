package org.bobo.service;

import org.bobo.bean.CategoryTagsDO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.dto.CategoryTagSetDTO;

/**
 * @Description: 视频分类标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface ICategoryTagsDOService extends IService<CategoryTagsDO> {
    /**
     * 设置视频分类标签
     * @param categoryTagSetDTO
     */
    void setCategoryTag(CategoryTagSetDTO categoryTagSetDTO);
}
