package org.bobo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.bobo.bean.VideoSetDO;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.dto.VideoSetDTO;
import org.bobo.vo.VideoSetVO;

import java.util.List;

/**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date: 2023-04-09
 * @Version: V1.0
 */
public interface IVideoSetDOService extends IService<VideoSetDO> {
    /**
     * 全查询 如果没传参数默认返回10条
     *
     * @param videoSetDTO 数据传输对象
     * @return 视频专辑列表
     */
    List<VideoSetVO> customList(VideoSetDTO videoSetDTO);

    /**
     * 今日热门查询
     *
     * @param videoSetDTO
     * @return List<VideoSetVO>
     */
    List<VideoSetVO> videoSetHostList(VideoSetDTO videoSetDTO);

    /**
     * 本周热门查询
     *
     * @param videoSetDTO
     * @return List<VideoSetVO>
     */
    List<VideoSetVO> videoSetHostListByWeek(VideoSetDTO videoSetDTO);

    /**
     * 设置为精选合集
     *
     * @param id
     * @return
     */
    void carefullyChosen(String id);

    /**
     * 取消精选
     *
     * @param id
     * @return
     */
    void unCarefullyChosen(String id);

    /**
     * 推荐
     *
     * @param videoSetDTO 参数
     * @return List<VideoSetDO>
     */
    IPage<VideoSetVO> recommend(Page<VideoSetDO> page, VideoSetDTO videoSetDTO);
    /**
     * 通过id查询
     *
     * @param id 专辑id
     * @return VideoSetVO
     */
    VideoSetVO customGetById(String id);
    /**
     * 分页列表查询
     *
     * @param videoSetDTO
     * @return
     */
    IPage<VideoSetVO> customPage(Page<VideoSetDO> page, VideoSetDTO videoSetDTO);

    /**
     * 返回推荐并携带其他和用户相关的参数
     * @param page
     * @param videoSetDTO
     * @return
     */
    IPage<VideoSetVO> recommendAuth(Page<VideoSetDO> page, VideoSetDTO videoSetDTO);
    /**
     * 点击量累积
     *
     * @param videoSetId
     * @param ipAddr
     * @return
     */
    void addClick(Long videoSetId, String ipAddr);
}
