package org.bobo.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.CollectDO;
import org.bobo.bean.UserDO;
import org.bobo.bean.VideoDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.common.constant.enums.BaseEnums;
import org.bobo.dto.ViewHistoryDTO;
import org.bobo.mapper.ViewHistoryDOMapper;
import org.bobo.service.ICollectDOService;
import org.bobo.service.IVideoDOService;
import org.bobo.service.IViewHistoryDOService;
import org.bobo.vo.ViewHistoryVO;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.View;
import java.math.BigDecimal;

/**
 * @Description: 播放历史记录管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Service
public class ViewHistoryDOServiceImpl extends ServiceImpl<ViewHistoryDOMapper, ViewHistoryDO> implements IViewHistoryDOService {


    @Autowired
    private IVideoDOService videoDOService;

    @Autowired
    private ICollectDOService collectDOService;
    /**
     * 记录观看历史
     *
     * @param viewHistoryDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void recordHistory(ViewHistoryDTO viewHistoryDTO) throws Exception {
        UserDO user=(UserDO) SecurityUtils.getSubject().getPrincipal();

        //查看是否已经存在当前用户和当前视频的记录
        ViewHistoryDO viewHistoryDB = baseMapper.selectOne(new LambdaQueryWrapper<ViewHistoryDO>().eq(ViewHistoryDO::getUserId, user.getId()).eq(ViewHistoryDO::getVideoId, viewHistoryDTO.getVideoId()).last(BaseEnums.LIMIT + " 1"));
        //播放进度
        BigDecimal schedule = countSchedule(viewHistoryDTO.getVideoId(), viewHistoryDTO.getTime());

        //如果存在则进行修改，不存在就新增
        if(viewHistoryDB!=null){
            viewHistoryDB.setTime(viewHistoryDTO.getTime());
            viewHistoryDB.setSchedule(schedule);
            baseMapper.updateById(viewHistoryDB);
        }else{
            ViewHistoryDO viewHistoryDO=new ViewHistoryDO();
            BeanUtil.copyProperties(viewHistoryDTO,viewHistoryDO);
            viewHistoryDO.setSchedule(schedule);
            viewHistoryDO.setUserId(user.getId());
            baseMapper.insert(viewHistoryDO);
        }

        //更新收藏的进度
        CollectDO collectDO = collectDOService.getOne(new LambdaQueryWrapper<CollectDO>().eq(CollectDO::getUserId, user.getId()).eq(CollectDO::getVideoId, viewHistoryDTO.getVideoId()).last(BaseEnums.LIMIT + " 1"));
        if(collectDO!=null){
            collectDO.setSchedule(schedule);
            collectDOService.updateById(collectDO);
        }
    }

    /**
     * 计算视频观看进度
     * @param videoId 视频id
     * @param time 观看到第几秒
     * @return
     */
    @Override
    public BigDecimal countSchedule(Long videoId,Integer time){
        VideoDO videoDO = videoDOService.getById(videoId);

        if(videoDO==null){
            return new BigDecimal("0");
        }
        //视频总秒数
        Integer videoDuration = videoDO.getVideoDuration();

        return new BigDecimal(time)
                .divide(new BigDecimal(videoDuration), 4, BigDecimal.ROUND_DOWN).multiply(new BigDecimal("100"));
    }
    /**
     * 分页列表查询
     *
     * @param viewHistoryDTO
     * @return
     */
    @Override
    public IPage<ViewHistoryVO> customPage(Page<ViewHistoryDO> page, ViewHistoryDTO viewHistoryDTO) {
        QueryWrapper<ViewHistoryDO> queryWrapper=viewHistoryDTO.initQueryWrapper(viewHistoryDTO);
        return baseMapper.customPage(page,queryWrapper);
    }
    /**
     * videoId
     *
     * @param videoId
     * @return ViewHistoryVO
     */
    @Override
    public Integer queryTime(Long videoId) {
        //获取
        UserDO user=(UserDO) SecurityUtils.getSubject().getPrincipal();

        ViewHistoryDO viewHistoryDB = baseMapper.selectOne(new LambdaQueryWrapper<ViewHistoryDO>().eq(ViewHistoryDO::getUserId, user.getId()).eq(ViewHistoryDO::getVideoId, videoId).last(BaseEnums.LIMIT + " 1"));
        if(viewHistoryDB!=null){
            return viewHistoryDB.getTime();
        }
        return 0;
    }
    /**
     * 获取当前视频合集观看到哪个视频了
     *
     * @param videoSetId
     * @return
     */
    @Override
    public Long getHistoryVideoByVideoSetId(Long videoSetId) {
        UserDO user=(UserDO) SecurityUtils.getSubject().getPrincipal();
        return baseMapper.getHistoryVideoByVideoSetId(videoSetId, user.getId());
    }
}
