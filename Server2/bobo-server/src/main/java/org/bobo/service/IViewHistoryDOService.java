package org.bobo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.dto.ViewHistoryDTO;
import org.bobo.vo.ViewHistoryVO;

import java.math.BigDecimal;

/**
 * @Description: 播放历史记录管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface IViewHistoryDOService extends IService<ViewHistoryDO> {
    /**
     * 记录观看历史
     *
     * @param viewHistoryDTO
     * @return
     */
    void recordHistory(ViewHistoryDTO viewHistoryDTO) throws Exception;
    /**
     * 计算视频观看进度
     * @param videoId 视频id
     * @param time 观看到第几秒
     * @return
     */
    BigDecimal countSchedule(Long videoId, Integer time);
    /**
     * 分页列表查询
     *
     * @param viewHistoryDTO
     * @return
     */
    IPage<ViewHistoryVO> customPage(Page<ViewHistoryDO> page, ViewHistoryDTO viewHistoryDTO);
    /**
     * videoId
     *
     * @param videoId
     * @return ViewHistoryVO
     */
    Integer queryTime(Long videoId);
    /**
     * 获取当前视频合集观看到哪个视频了
     *
     * @param videoSetId
     * @return
     */
    Long getHistoryVideoByVideoSetId(Long videoSetId);
}
