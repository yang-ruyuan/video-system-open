package org.bobo.service;

import org.bobo.bean.VideoSetTagsDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 视频合集标签管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
public interface IVideoSetTagsDOService extends IService<VideoSetTagsDO> {

}
