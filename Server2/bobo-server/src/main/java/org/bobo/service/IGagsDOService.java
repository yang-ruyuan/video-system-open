package org.bobo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.GagsDO;
import org.bobo.dto.GagsDTO;
import org.bobo.vo.GagsVO;

/**
 * @Description: 花絮管理
 * @Author: boBo
 * @Date:   2023-06-07
 * @Version: V1.0
 */
public interface IGagsDOService extends IService<GagsDO> {
    /**
     * 添加
     *
     * @param gagsDTO
     * @return
     */
    void saveDTO(GagsDTO gagsDTO);
    /**
     * 分页列表查询
     *
     * @param page
     * @param gagsDTO
     * @return
     */
    IPage<GagsVO> customPage(Page<GagsDO> page, GagsDTO gagsDTO);
}
