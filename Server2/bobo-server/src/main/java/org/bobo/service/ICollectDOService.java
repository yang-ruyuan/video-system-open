package org.bobo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.CollectDO;
import org.bobo.common.api.vo.Result;
import org.bobo.dto.CollectDTO;
import org.bobo.vo.CollectVO;

/**
 * @Description: 收藏管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
public interface ICollectDOService extends IService<CollectDO> {
    /**
     * 收藏或取消收藏
     *
     * @param collectDTO
     * @return
     */
    Result<?> saveOrDel(CollectDTO collectDTO);
    /**
     * 查看是否收藏
     *
     * @param videoId
     * @return
     */
    Result<?> check(Long videoId);
    /**
     * 分页列表查询
     *
     * @param collectDTO
     * @return
     */
    IPage<CollectVO> customPage(Page<CollectDO> page, CollectDTO collectDTO);
}
