package org.bobo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.bobo.bean.CommentLikeRecordDO;

/**
 * @Description: 评论点赞记录
 * @Author: boBo
 * @Date:   2023-06-17
 * @Version: V1.0
 */
public interface ICommentLikeRecordDOService extends IService<CommentLikeRecordDO> {
    /**
     * 根据用户ID和评论ID查询
     * @param commentId 评论id
     * @param userId 用户id
     * @return
     */
    CommentLikeRecordDO getByUserIdCommentId(Long commentId, Integer userId);
}
