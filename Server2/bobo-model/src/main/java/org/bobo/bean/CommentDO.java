package org.bobo.bean;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @Description: 文章评论管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
@Data
@TableName("v_comment")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_comment对象", description="评论管理")
public class CommentDO {
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private Long id;
	/**评论用户id*/
	@Excel(name = "评论用户id", width = 15)
    @ApiModelProperty(value = "评论用户id")
	private Integer userId;
	/**源ID*/
	@Excel(name = "源ID", width = 15)
	@ApiModelProperty(value = "源ID")
	private Long sourceId;
	/**评论内容*/
	@Excel(name = "评论内容", width = 15)
    @ApiModelProperty(value = "评论内容")
	@NotEmpty(message = "评论内容不能为空")
	private String content;
	/**评论类型*/
	@Excel(name = "评论类型", width = 15)
	@ApiModelProperty(value = "评论类型")
	private String type;
	/**父id，回复用*/
	@Excel(name = "父id，回复用", width = 15)
    @ApiModelProperty(value = "父id，回复用")
	private Long parentId;
	/**回复数*/
	@Excel(name = "回复数", width = 15)
	@ApiModelProperty(value = "回复数")
	private Integer replyCount;
	/**评论者ip*/
	@Excel(name = "评论者ip", width = 15)
	@ApiModelProperty(value = "评论者ip")
	private String ip;
	/**对应地址*/
	@Excel(name = "对应地址", width = 15)
	@ApiModelProperty(value = "对应地址")
	private String address;
	/**点赞数*/
	@Excel(name = "点赞数", width = 15)
	@ApiModelProperty(value = "点赞数")
	private Integer likeNum;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**修改时间*/
	@Excel(name = "修改时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.UPDATE)
	private Date updateTime;
	/**reply*/
	@Excel(name = "reply", width = 15)
    @ApiModelProperty(value = "reply")
	private Long reply;

	/**回复谁*/
	@Excel(name = "回复谁", width = 15)
	@ApiModelProperty(value = "回复谁")
	private Integer replyWho;

	@TableField(exist = false)
	private List<CommentDO> sonComment;

	@TableField(exist = false)
	private String realName;
	@TableField(exist = false)
	private String avatar;

	@TableField(exist = false)
	private Integer sonCount;
}
