package org.bobo.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 收藏管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@TableName("v_collect")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_collect对象", description="收藏管理")
public class CollectDO {
    
	/**收藏管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "收藏管理")
	private Long id;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
	private Integer userId;
	/**播放进度（%）*/
	@Excel(name = "播放进度（%）", width = 15)
    @ApiModelProperty(value = "播放进度（%）")
	private BigDecimal schedule;
	/**播放时间（s）*/
	@Excel(name = "播放时间（s）", width = 15)
    @ApiModelProperty(value = "播放时间（s）")
	private Integer time;
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	private Long videoId;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
}
