package org.bobo.bean;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Description: 视频管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Data
@TableName("v_video")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_video对象", description="视频管理")
public class VideoDO {
    
	/**视频管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "视频管理")
	private Integer id;
	/**视频名称*/
	@Excel(name = "视频名称", width = 15)
    @ApiModelProperty(value = "视频名称")
	@NotEmpty(message = "视频名称不允许为空")
	private String name;
	/**视频合集id*/
	@Excel(name = "视频合集id", width = 15)
    @ApiModelProperty(value = "视频合集id")
	@NotNull(message = "视频所属专辑不允许为空")
	private Integer videoSetId;
	/**视频封面水平*/
	@Excel(name = "视频封面水平", width = 15)
    @ApiModelProperty(value = "视频封面水平")
	@NotEmpty(message = "视频封面水平不允许为空")
	private String coverLevel;
	/**视频封面垂直*/
	@Excel(name = "视频封面垂直", width = 15)
	@ApiModelProperty(value = "视频封面垂直")
	@NotEmpty(message = "视频垂直封面不允许为空")
	private String coverVertical;
	/**视频本地存储路径*/
	@Excel(name = "视频本地存储路径", width = 15)
    @ApiModelProperty(value = "视频本地存储路径")
	@NotEmpty(message = "视频不允许为空")
	private String playUrl;
	/**简短简介*/
	@Excel(name = "简短简介", width = 15)
    @ApiModelProperty(value = "简短简介")
	@NotEmpty(message = "视频简介不允许为空")
	private String introductionShort;
	/**在合集内的排序*/
	@Excel(name = "在合集内的排序", width = 15)
    @ApiModelProperty(value = "在合集内的排序")
	@NotNull(message = "视频排序不允许为空")
	private Integer sort;
	/**视频时长 秒*/
	@Excel(name = "视频时长 秒", width = 15)
    @ApiModelProperty(value = "视频时长 秒")
	@NotNull(message = "视频时长不允许为空")
	private Integer videoDuration;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**updateTime*/
	@Excel(name = "updateTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "updateTime")
	@TableField(fill = FieldFill.UPDATE)
	private Date updateTime;
}
