package org.bobo.bean;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 专辑点击记录
 * @Author: boBo
 * @Date:   2023-07-23
 * @Version: V1.0
 */
@Data
@TableName("v_video_set_click_record")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_video_set_click_record对象", description="专辑点击记录")
public class VideoSetClickRecordDO {
    
	/**专辑点击记录*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "专辑点击记录")
	private Integer id;
	/**ip*/
	@Excel(name = "ip", width = 15)
    @ApiModelProperty(value = "ip")
	private String ipAddress;
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	private Long videoSetId;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
}
