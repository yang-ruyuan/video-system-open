package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date:   2022-02-27
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="cl_user传输层对象", description="用户管理传输层对象")
public class UserDTO {
    
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private Integer id;
	/**avatar*/
	@Excel(name = "avatar", width = 15)
    @ApiModelProperty(value = "avatar")
	private String avatar;
	/**realName*/
	@Excel(name = "realName", width = 15)
    @ApiModelProperty(value = "realName")
	private String realName;
}
