package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.bobo.bean.ChasingDramaDO;
import org.bobo.bean.ViewHistoryDO;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: 追剧管理管理 传输层对象
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data

@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_chasing_drama传输层对象", description="追剧管理管理 传输层对象")
public class ChasingDramaDTO {

	public QueryWrapper<ChasingDramaDO> initQueryWrapper(ChasingDramaDTO chasingDramaDTO) {
		QueryWrapper<ChasingDramaDO> queryWrapper=new QueryWrapper<>();
		queryWrapper.orderByDesc("cd.create_time");
		return queryWrapper;
	}
}
