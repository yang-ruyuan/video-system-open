package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.UserDO;
import org.bobo.bean.ViewHistoryDO;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Description: 播放历史记录传输层对象
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@TableName("v_view_history")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_view_history传输层对象", description="播放历史记录传输层对象")
public class ViewHistoryDTO {
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	@NotNull(message = "视频id不允许为空")
	private Long videoId;
	/**观看时长（s）*/
    @ApiModelProperty(value = "观看时长（s）")
	@NotNull(message = "观看时长不允许为空")
	private Integer time;

	public QueryWrapper<ViewHistoryDO> initQueryWrapper(ViewHistoryDTO viewHistoryDTO) {
		QueryWrapper<ViewHistoryDO> queryWrapper=new QueryWrapper<>();
		UserDO user=(UserDO) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("h.user_id",user.getId());
		return queryWrapper;
	}
}
