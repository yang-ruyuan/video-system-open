package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.bobo.bean.VideoSetDO;
import org.bobo.common.constant.enums.BaseEnums;
import org.bobo.common.util.oConvertUtils;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date: 2023-04-09
 * @Version: V1.0
 */
@Data

@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "v_video_set数据传输对象", description = "数据传输对象")
public class VideoSetDTO {
    private Integer id;

    @ApiModelProperty(value = "集合名称")
    private String setName;

    @ApiModelProperty(value = "合集分类名称")
    private String categoryName;

    @ApiModelProperty(value = "合集长简介")
    private Object setIntroductionLong;

    @ApiModelProperty(value = "合集短简介")
    private String setIntroductionShort;

    /**
     * 视频封面水平
     */
    @ApiModelProperty(value = "视频封面水平")
    @NotEmpty(message = "视频封面水平不允许为空")
    private String coverLevel;
    /**
     * 视频封面垂直
     */
    @Excel(name = "视频封面垂直", width = 15)
    @ApiModelProperty(value = "视频封面垂直")
    @NotEmpty(message = "视频垂直封面不允许为空")
    private String coverVertical;

    @ApiModelProperty(value = "标签")
    private String tags;

    @ApiModelProperty(value = "预览视频地址")
    private String coverVideoUrl;

    @ApiModelProperty(value = "获取几条数据")
    private String limit;

    @ApiModelProperty(value = "是否精选 0否 1是")
    private String carefullyChosen;


    public LambdaQueryWrapper<VideoSetDO> initQueryWrapper(VideoSetDTO videoSetDTO) {
        LambdaQueryWrapper<VideoSetDO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        //根据专辑分类查询
        lambdaQueryWrapper.eq(oConvertUtils.isNotEmpty(videoSetDTO.getCategoryName()), VideoSetDO::getCategoryName, videoSetDTO.getCategoryName());
        //根据专辑名称模糊查询
        lambdaQueryWrapper.like(oConvertUtils.isNotEmpty(videoSetDTO.getSetName()), VideoSetDO::getSetName, videoSetDTO.getSetName());
        //动态limit
        lambdaQueryWrapper.last(oConvertUtils.isNotEmpty(videoSetDTO.getLimit()), BaseEnums.LIMIT.getValue() + " " + videoSetDTO.getLimit());
        //是否精选
        lambdaQueryWrapper.eq(oConvertUtils.isNotEmpty(videoSetDTO.getCarefullyChosen()), VideoSetDO::getCarefullyChosen, videoSetDTO.getCarefullyChosen());
        //排序
        lambdaQueryWrapper.orderByDesc(VideoSetDO::getCreateTime);
        //标签
        if (oConvertUtils.isNotEmpty(videoSetDTO.getTags())) {
            for (String tag : videoSetDTO.getTags().split("\\|")) {
                String[] split = tag.split(",");
                StringBuilder sql = new StringBuilder("(");
                for (int i = 0; i < split.length; i++) {
                    int finalI = i;
                    if (finalI == 0) {
                        sql.append("FIND_IN_SET('").append(split[finalI]).append("', tags)");
                    } else {
                        sql.append(" OR FIND_IN_SET('").append(split[finalI]).append("', tags)");
                    }
                }
                lambdaQueryWrapper.apply(sql + ")");
            }
        }

        return lambdaQueryWrapper;
    }
}
