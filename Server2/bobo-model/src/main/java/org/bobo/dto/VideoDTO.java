package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.bobo.bean.VideoDO;
import org.bobo.common.util.oConvertUtils;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Description: 视频管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_video传输层对象", description="视频管理传输层")
public class VideoDTO {

	@ApiModelProperty(value = "视频名称")
	@NotEmpty(message = "视频名称不允许为空")
	private String name;

	@ApiModelProperty(value = "视频合集id")
	@NotNull(message = "视频所属专辑不允许为空")
	private Integer videoSetId;

	/**视频封面水平*/
	@Excel(name = "视频封面水平", width = 15)
	@ApiModelProperty(value = "视频封面水平")
	@NotEmpty(message = "视频封面水平不允许为空")
	private String coverLevel;

	/**视频封面垂直*/
	@Excel(name = "视频封面垂直", width = 15)
	@ApiModelProperty(value = "视频封面垂直")
	@NotEmpty(message = "视频垂直封面不允许为空")
	private String coverVertical;

	@ApiModelProperty(value = "视频本地存储路径")
	@NotEmpty(message = "视频不允许为空")
	private String playUrl;

	@ApiModelProperty(value = "简短简介")
	@NotEmpty(message = "视频简介不允许为空")
	private String introductionShort;

	@ApiModelProperty(value = "在合集内的排序")
	@NotNull(message = "视频排序不允许为空")
	private Integer sort;

	@ApiModelProperty(value = "视频时长 秒")
	@NotNull(message = "视频时长不允许为空")
	private Integer videoDuration;

	public LambdaQueryWrapper<VideoDO> initQueryWrapper(VideoDTO videoDTO) {
		LambdaQueryWrapper<VideoDO> lambdaQueryWrapper=new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(oConvertUtils.isNotEmpty(videoDTO.getVideoSetId()),VideoDO::getVideoSetId,videoDTO.getVideoSetId());
		lambdaQueryWrapper.eq(oConvertUtils.isNotEmpty(videoDTO.getName()),VideoDO::getName,videoDTO.getName());
		lambdaQueryWrapper.orderByDesc(VideoDO::getCreateTime);
		return lambdaQueryWrapper;
	}
}
