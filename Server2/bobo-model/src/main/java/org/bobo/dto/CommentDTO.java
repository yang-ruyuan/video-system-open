package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @Description: 评论管理
 * @Author: bobo
 * @Date:   2023-06-12
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_comment传输层对象", description="传输层对象")
public class CommentDTO {
	/**id*/
    @ApiModelProperty(value = "id")
	private Long id;
	/**评论用户id*/
    @ApiModelProperty(value = "评论用户id")
	private Integer userId;
	/**源ID*/
    @ApiModelProperty(value = "源ID")
	@NotNull(message = "sourceId不能为空")
	private Long sourceId;
	/**评论内容*/
    @ApiModelProperty(value = "评论内容")
	@NotEmpty(message = "评论内容不能为空")
	private String content;
	/**评论类型*/
	@ApiModelProperty(value = "评论类型")
	@NotEmpty(message = "评论类型不能为空")
	private String type;
	/**评论者ip*/
	@Excel(name = "评论者ip", width = 15)
	@ApiModelProperty(value = "评论者ip")
	private String ip;
	/**父id，回复用*/
	@Excel(name = "父id，回复用", width = 15)
    @ApiModelProperty(value = "父id，回复用")
	private Long parentId;
	/**reply*/
	@Excel(name = "reply", width = 15)
    @ApiModelProperty(value = "回复哪条评论")
	private Long reply;
}
