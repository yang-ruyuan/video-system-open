package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.bobo.bean.GagsDO;
import org.bobo.common.util.oConvertUtils;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Description: 花絮管理传输层
 * @Author: boBo
 * @Date: 2023-06-07
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "v_gags传输层对象", description = "花絮管理传输层")
public class GagsDTO {
    /**
     * 视频合集id
     */
    @Excel(name = "视频合集id", width = 15)
    @ApiModelProperty(value = "视频合集id")
    @NotNull(message = "视频合集id不能为空")
    private Integer videoSetId;
    /**
     * 花絮名称
     */
    @Excel(name = "花絮名称", width = 15)
    @ApiModelProperty(value = "花絮名称")
    @NotEmpty(message = "花絮名称不允许为空")
    private String name;
    /**视频封面水平*/
    @Excel(name = "视频封面水平", width = 15)
    @ApiModelProperty(value = "视频封面水平")
    @NotEmpty(message = "视频封面水平不允许为空")
    private String coverLevel;
    /**视频封面垂直*/
    @Excel(name = "视频封面垂直", width = 15)
    @ApiModelProperty(value = "视频封面垂直")
    @NotEmpty(message = "视频垂直封面不允许为空")
    private String coverVertical;
    /**
     * 视频地址
     */
    @Excel(name = "视频地址", width = 15)
    @ApiModelProperty(value = "视频地址")
    @NotEmpty(message = "视频地址不允许为空")
    private String playUrl;

    public LambdaQueryWrapper<GagsDO> initQueryWrapper(GagsDTO gagsDTO) {
        LambdaQueryWrapper<GagsDO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(oConvertUtils.isNotEmpty(gagsDTO.getVideoSetId()),GagsDO::getVideoSetId,gagsDTO.getVideoSetId());
        return lambdaQueryWrapper;
    }
}
