package org.bobo.dto;

import lombok.Data;
import org.bobo.bean.CategoryTagsDO;
import org.checkerframework.checker.units.qual.C;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryTagSetDTO {

    @NotEmpty(message = "视频分类名称不允许为空")
    private String categoryName;

    private List<List<String>> tagList;


    public List<CategoryTagsDO> dtoTODOS(CategoryTagSetDTO categoryTagSetDTO){
        List<CategoryTagsDO> categoryTagsDOS=new ArrayList<>();

        if(categoryTagSetDTO.tagList.size()>0){
            categoryTagSetDTO.tagList.forEach(item ->{
                CategoryTagsDO categoryTagsDO=new CategoryTagsDO();
                categoryTagsDO.setCategoryName(categoryTagSetDTO.getCategoryName());
                categoryTagsDO.setTags(String.join(",",item));
                categoryTagsDOS.add(categoryTagsDO);
            });
        }

        return categoryTagsDOS;
    }
}
