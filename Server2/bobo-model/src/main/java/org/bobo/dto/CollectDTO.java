package org.bobo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.shiro.SecurityUtils;
import org.bobo.bean.CollectDO;
import org.bobo.bean.UserDO;
import org.bobo.bean.ViewHistoryDO;
import org.bobo.vo.CollectVO;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 收藏管理 传输层
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@TableName("v_collect")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_collect传输层对象", description="收藏管理传输层")
public class CollectDTO {
	/**播放时间（s）*/
	@Excel(name = "播放时间（s）", width = 15)
    @ApiModelProperty(value = "播放时间（s）")
	private Integer time;
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	@NotNull(message = "视频id不允许为空")
	private Long videoId;

	public QueryWrapper<CollectDO> initQueryWrapper(CollectDTO collectDTO) {
		QueryWrapper<CollectDO> queryWrapper=new QueryWrapper<>();
		UserDO user=(UserDO) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("c.user_id",user.getId());
		return queryWrapper;
	}
}
