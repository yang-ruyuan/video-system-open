package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * @Description: 视频合集管理
 * @Author: boBo
 * @Date:   2023-04-09
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_video_set视图对象", description="视频合集管理视图对象")
public class VideoSetVO {
	/**视频集合管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "视频集合管理")
	private Integer id;
	/**集合名称*/
	@Excel(name = "集合名称", width = 15)
    @ApiModelProperty(value = "集合名称")
	private String setName;
	/**合集分类名称*/
	@Excel(name = "合集分类名称", width = 15)
    @ApiModelProperty(value = "合集分类名称")
	private String categoryName;
	/**合集长简介*/
	@Excel(name = "合集长简介", width = 15)
    @ApiModelProperty(value = "合集长简介")
	private Object setIntroductionLong;
	/**合集短简介*/
	@Excel(name = "合集短简介", width = 15)
    @ApiModelProperty(value = "合集短简介")
	private String setIntroductionShort;
	/**视频封面水平*/
	@Excel(name = "视频封面水平", width = 15)
	@ApiModelProperty(value = "视频封面水平")
	@NotEmpty(message = "视频封面水平不允许为空")
	private String coverLevel;
	/**视频封面垂直*/
	@Excel(name = "视频封面垂直", width = 15)
	@ApiModelProperty(value = "视频封面垂直")
	@NotEmpty(message = "视频垂直封面不允许为空")
	private String coverVertical;
	/**预览视频地址*/
	@Excel(name = "预览视频地址", width = 15)
    @ApiModelProperty(value = "预览视频地址")
	private String coverVideoUrl;
	/**是否精选 0否 1是*/
	@Excel(name = "是否精选 0否 1是", width = 15)
	@ApiModelProperty(value = "是否精选 0否 1是")
	private String carefullyChosen;
	/**标签*/
	@Excel(name = "标签", width = 15)
	@ApiModelProperty(value = "标签")
	private String tags;
	/**是否收藏*/
	@Excel(name = "是否收藏", width = 15)
	@ApiModelProperty(value = "是否收藏")
	private boolean chasingDrama;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	private Date createTime;
	/**updateTime*/
	@Excel(name = "updateTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "updateTime")
	private Date updateTime;
}
