package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * @Description: 花絮管理视图
 * @Author: boBo
 * @Date:   2023-06-07
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_gags视图对象", description="花絮管理视图")
public class GagsVO {
    
	/**花絮管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "花絮管理")
	private Integer id;
	/**视频合集id*/
	@Excel(name = "视频合集id", width = 15)
    @ApiModelProperty(value = "视频合集id")
	private Integer videoSetId;
	/**花絮名称*/
	@Excel(name = "花絮名称", width = 15)
    @ApiModelProperty(value = "花絮名称")
	private String name;
	/**视频封面水平*/
	@Excel(name = "视频封面水平", width = 15)
	@ApiModelProperty(value = "视频封面水平")
	@NotEmpty(message = "视频封面水平不允许为空")
	private String coverLevel;
	/**视频封面垂直*/
	@Excel(name = "视频封面垂直", width = 15)
	@ApiModelProperty(value = "视频封面垂直")
	@NotEmpty(message = "视频垂直封面不允许为空")
	private String coverVertical;
	/**视频地址*/
	@Excel(name = "视频地址", width = 15)
    @ApiModelProperty(value = "视频地址")
	private String playUrl;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	private Date createTime;
	/**updateTime*/
	@Excel(name = "updateTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "updateTime")
	private Date updateTime;

	private VideoSetVO videoSetVO;
}
