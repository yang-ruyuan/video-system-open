package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 播放历史记录管理视图
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_view_history视图对象", description="播放历史记录管理视图")
public class ViewHistoryVO {
    
	/**视频观看历史*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "视频观看历史")
	private Long id;
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	private Long videoId;
	/**视频集合id*/
	@Excel(name = "视频集合id", width = 15)
	@ApiModelProperty(value = "视频集合id")
	private Long setId;
	/**视频名称*/
	@Excel(name = "视频名称", width = 15)
	@ApiModelProperty(value = "视频名称")
	private String videoName;
	/**视频专辑名称*/
	@Excel(name = "视频专辑名称", width = 15)
	@ApiModelProperty(value = "视频专辑名称")
	private String videoSetName;
	/**视频封面(水平)*/
	@Excel(name = "视频封面(水平)", width = 15)
	@ApiModelProperty(value = "视频封面(水平)")
	private String coverLevel;
	/**视频封面(垂直)*/
	@Excel(name = "视频封面(垂直)", width = 15)
	@ApiModelProperty(value = "视频封面(垂直)")
	private String coverVertical;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
	private Integer userId;
	/**观看进度（%）*/
	@Excel(name = "观看进度（%）", width = 15)
    @ApiModelProperty(value = "观看进度（%）")
	private BigDecimal schedule;
	/**观看时长（s）*/
    @ApiModelProperty(value = "观看时长（s）")
	private Integer time;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**修改时间*/
	@Excel(name = "修改时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
}
