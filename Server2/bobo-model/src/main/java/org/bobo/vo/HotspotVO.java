package org.bobo.vo;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 热点表/热搜表 视图
 * @Author: boBo
 * @Date:   2022-03-26
 * @Version: V1.0
 */
@Data
@TableName("cl_hotspot_vo")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="cl_hotspot_vo对象", description="热点表/热搜表 视图")
public class HotspotVO {
    
	/**热点表/热搜表*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "热点表/热搜表")
	private Integer id;
	/**热点名称*/
	@Excel(name = "热点名称", width = 15)
    @ApiModelProperty(value = "热点名称")
	private String title;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	private Date createTime;
	/**updateTime*/
	@Excel(name = "updateTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "updateTime")
	private Date updateTime;
	/**count*/
	@Excel(name = "count", width = 15)
    @ApiModelProperty(value = "count")
	private Integer count;
}
