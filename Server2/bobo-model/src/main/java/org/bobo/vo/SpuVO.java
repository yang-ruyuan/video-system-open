package org.bobo.vo;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 商品视图
 * @Author: boBo
 * @Date:   2022-04-05
 * @Version: V1.0
 */
@Data
@TableName("cl_spu_vo")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="cl_spu_vo对象", description="商品视图")
public class SpuVO {
    
	/**商品管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "商品管理")
	private Integer id;
	/**商品名称 15个字符限制*/
	@Excel(name = "商品名称 15个字符限制", width = 15)
    @ApiModelProperty(value = "商品名称 15个字符限制")
	private String title;
	/**商品单价 限制5位整数+2位小数*/
	@Excel(name = "商品单价 限制5位整数+2位小数", width = 15)
    @ApiModelProperty(value = "商品单价 限制5位整数+2位小数")
	private java.math.BigDecimal price;
	/**商品分类id*/
	@Excel(name = "商品分类id", width = 15)
    @ApiModelProperty(value = "商品分类id")
	private Integer categoryId;
	/**库存 限制5位整数*/
	@Excel(name = "库存 限制5位整数", width = 15)
    @ApiModelProperty(value = "库存 限制5位整数")
	private Integer stock;
	/**销量 限制6位整数*/
	@Excel(name = "销量 限制6位整数", width = 15)
    @ApiModelProperty(value = "销量 限制6位整数")
	private Integer salesVolume;
	/**单位 限制20个字符*/
	@Excel(name = "单位 限制20个字符", width = 15)
    @ApiModelProperty(value = "单位 限制20个字符")
	private String unit;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	private Date createTime;
	/**updateTime*/
	@Excel(name = "updateTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "updateTime")
	private Date updateTime;
	/**是否删除 1删除 0未删除*/
	@Excel(name = "是否删除 1删除 0未删除", width = 15)
    @ApiModelProperty(value = "是否删除 1删除 0未删除")
	private Integer isDeleted;
	/**商品详细描述 限制255个字符*/
	@Excel(name = "商品详细描述 限制255个字符", width = 15)
    @ApiModelProperty(value = "商品详细描述 限制255个字符")
	private String detail;
	/**店铺id*/
	@Excel(name = "店铺id", width = 15)
    @ApiModelProperty(value = "店铺id")
	private String shopId;
	/**商品图片 限制2550个字符*/
	@Excel(name = "商品图片 限制2550个字符", width = 15)
    @ApiModelProperty(value = "商品图片 限制2550个字符")
	private String imgs;

	/**前端选择数量*/
	@TableField(exist = false)
	@ApiModelProperty(value = "前端选择数量 这个参数给前端用 前端就不用循环给对象加参数了")
	private Integer num=0;
}
