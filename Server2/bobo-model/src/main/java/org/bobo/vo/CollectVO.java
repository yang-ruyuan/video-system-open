package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 收藏管理视图
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_collect视图对象", description="收藏管理视图")
public class CollectVO {
    
	/**收藏管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "收藏管理")
	private Long id;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
	private Integer userId;
	/**播放进度（%）*/
	@Excel(name = "播放进度（%）", width = 15)
    @ApiModelProperty(value = "播放进度（%）")
	private BigDecimal schedule;
	/**播放时间（s）*/
	@Excel(name = "播放时间（s）", width = 15)
    @ApiModelProperty(value = "播放时间（s）")
	private Integer time;
	/**视频id*/
	@Excel(name = "视频id", width = 15)
    @ApiModelProperty(value = "视频id")
	private Long videoId;
	/**视频集合id*/
	@Excel(name = "视频集合id", width = 15)
	@ApiModelProperty(value = "视频集合id")
	private Long setId;
	/**视频名称*/
	@Excel(name = "视频名称", width = 15)
	@ApiModelProperty(value = "视频名称")
	private String videoName;
	/**视频专辑名称*/
	@Excel(name = "视频专辑名称", width = 15)
	@ApiModelProperty(value = "视频专辑名称")
	private String videoSetName;
	/**视频封面(水平)*/
	@Excel(name = "视频封面(水平)", width = 15)
	@ApiModelProperty(value = "视频封面(水平)")
	private String coverLevel;
	/**视频封面(垂直)*/
	@Excel(name = "视频封面(垂直)", width = 15)
	@ApiModelProperty(value = "视频封面(垂直)")
	private String coverVertical;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
}
