package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.bobo.bean.UserDO;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

/**
 * @Description: 文章评论管理
 * @Author: bobo
 * @Date:   2022-02-27
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_comment视图对象", description="视图对象")
public class CommentVO {
	/**id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
	private Long id;
	/**评论用户id*/
	@Excel(name = "评论用户id", width = 15)
    @ApiModelProperty(value = "评论用户id")
	private Integer userId;
	/**源ID*/
	@Excel(name = "源ID", width = 15)
	@ApiModelProperty(value = "源ID")
	private Long sourceId;
	/**评论内容*/
	@Excel(name = "评论内容", width = 15)
    @ApiModelProperty(value = "评论内容")
	@NotEmpty(message = "评论内容不能为空")
	private String content;
	/**评论类型*/
	@Excel(name = "评论类型", width = 15)
	@ApiModelProperty(value = "评论类型")
	private String type;
	/**父id，回复用*/
	@Excel(name = "父id，回复用", width = 15)
    @ApiModelProperty(value = "父id，回复用")
	private Integer parentId;
	/**回复数*/
	@Excel(name = "回复数", width = 15)
	@ApiModelProperty(value = "回复数")
	private Integer replyCount;
	/**评论者ip*/
	@Excel(name = "评论者ip", width = 15)
	@ApiModelProperty(value = "评论者ip")
	private String ip;
	/**对应地址*/
	@Excel(name = "对应地址", width = 15)
	@ApiModelProperty(value = "对应地址")
	private String address;
	/**点赞数*/
	@Excel(name = "点赞数", width = 15)
	@ApiModelProperty(value = "点赞数")
	private Integer likeNum;
	/**回复哪一条评论*/
	@Excel(name = "回复哪一条评论", width = 15)
	@ApiModelProperty(value = "回复哪一条评论")
	private Long reply;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**修改时间*/
	@Excel(name = "修改时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
	private Date updateTime;

	/**回复谁*/
	@ApiModelProperty(value = "回复谁")
	private UserVO replyWho;

	/**评论者*/
	@ApiModelProperty(value = "评论者")
	private UserVO commentWho;

	/**我点赞的*/
	@ApiModelProperty(value = "我点赞的")
	private boolean myLike;
}
