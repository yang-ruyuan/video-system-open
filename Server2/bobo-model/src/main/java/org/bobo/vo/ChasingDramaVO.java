package org.bobo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: 追剧管理管理
 * @Author: boBo
 * @Date:   2023-06-03
 * @Version: V1.0
 */
@Data
@TableName("v_chasing_drama")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="v_chasing_drama对象", description="追剧管理管理")
public class ChasingDramaVO {
    
	/**追剧管理*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "追剧管理")
	private Integer id;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
	private Integer userId;
	/**视频专辑id*/
	@Excel(name = "视频专辑id", width = 15)
    @ApiModelProperty(value = "视频专辑id")
	private Long videoSetId;
	/**专辑名称*/
	@Excel(name = "专辑名称", width = 15)
	@ApiModelProperty(value = "专辑名称")
	private String setName;
	/**专辑断简介*/
	@Excel(name = "专辑断简介", width = 15)
	@ApiModelProperty(value = "专辑断简介")
	private String setIntroductionShort;
	/**专辑封面(水平)*/
	@Excel(name = "专辑封面(水平)", width = 15)
	@ApiModelProperty(value = "专辑封面(水平)")
	private String coverLevel;
	/**视频封面(垂直)*/
	@Excel(name = "专辑封面(垂直)", width = 15)
	@ApiModelProperty(value = "专辑封面(垂直)")
	private String coverVertical;
	/**createTime*/
	@Excel(name = "createTime", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "createTime")
	private Date createTime;
}
